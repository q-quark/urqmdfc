/**
 ** This file is part of the urqmdfc project.
 ** Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef URQMD_PARTICLE_H
#define URQMD_PARTICLE_H

#include <experimental/optional>

#include "utils.h"
#include "line_stream.h"
#include "urqmd_types.h"

class urqmd_basicparticle
{
public:
    urqmd_basicparticle()
        : m_t{0},
          m_x{0},
          m_y{0},
          m_z{0},
          m_E{0},
          m_px{0},
          m_py{0},
          m_pz{0},
          m_mass{0},
          m_ityp{0}, // particle type
          m_dIz{0}, // (doubled) Isospin z-projection
          m_chg{0}, // charge
          m_lcl{0}, // idx last collision partner
          m_Ncol{0} // number of collisions
    { }

    double t() const { return m_t; }
    double x() const { return m_x; }
    double y() const { return m_y; }
    double z() const { return m_z; }
    double energy() const { return m_E; }
    double px() const { return m_px; }
    double py() const { return m_py; }
    double pz() const { return m_pz; }
    double mass() const { return m_mass; }
    int ityp() const { return m_ityp; }
    int iso3() const { return m_dIz; }
    int charge() const { return m_chg; }
    int collision_parent() const { return m_lcl; }
    uint collisions() const { return m_Ncol; }

protected:
    double m_t;
    double m_x;
    double m_y;
    double m_z;
    double m_E;
    double m_px;
    double m_py;
    double m_pz;
    double m_mass;
    int m_ityp; // particle type
    int m_dIz; // (doubled) Isospin z-projection
    int m_chg; // charge
    int m_lcl; // parent collision number
    uint m_Ncol; // number of collisions
};


template <urqmd_filetype FileType>
class urqmd_particle
{
public:
    urqmd_particle() { }

};

template <urqmd_filetype FileType>
line_stream& operator>>(line_stream&, urqmd_particle<FileType>&);

//------ URQMD FILE 13 ---
template <>
class urqmd_particle<urqmd_filetype::F13> : public urqmd_basicparticle
{
    friend line_stream& operator>> <urqmd_filetype::F13>(line_stream&, urqmd_particle&);
public:
    urqmd_particle()
        : m_ppr{0}, // parent process type
          m_time_fr{0}, // freeze-out time of particle
          m_x_fr{0}, // freeze-out x position of particle
          m_y_fr{0}, // freeze-out y position of particle
          m_z_fr{0}, // freeze-out z position of particle
          m_E_fr{0}, // freeze-out energy of particle
          m_px_fr{0}, // freeze-out px of particle
          m_py_fr{0}, // freeze-out py of particle
          m_pz_fr{0} // freeze-out pz of particle
    { }

    int parent_proc_id() const { return m_ppr; }

    double freezeout_time() const { return m_time_fr; }
    double freezeout_x() const { return m_x_fr; }
    double freezeout_y() const { return m_y_fr; }
    double freezeout_z() const { return m_z_fr; }
    double freezeout_energy() const { return m_E_fr; }
    double freezeout_px() const { return m_px_fr; }
    double freezeout_py() const { return m_py_fr; }
    double freezeout_pz() const { return m_pz_fr; }

private:
    int m_ppr; // parent process type
    double m_time_fr; // freeze-out time of particle
    double m_x_fr; // freeze-out x position of particle
    double m_y_fr; // freeze-out y position of particle
    double m_z_fr; // freeze-out z position of particle
    double m_E_fr; // freeze-out energy of particle
    double m_px_fr; // freeze-out px of particle
    double m_py_fr; // freeze-out py of particle
    double m_pz_fr; // freeze-out pz of particle
};

template <>
line_stream& operator>>(line_stream& is, urqmd_particle<urqmd_filetype::F13>& particle)
{
    std::string line;

    // parse frame info
    // check a possible good frame header
    if( !(getline(is,line) && line.length()==307) )
    {
        is.unget();
        is.setstate(std::ios_base::failbit);
        return is;
    }

    particle.m_t = std::stod( line.substr(0,16) );
    particle.m_x = std::stod( line.substr(16,16) );
    particle.m_y = std::stod( line.substr(32,16) );
    particle.m_z = std::stod( line.substr(48,16) );
    particle.m_E = std::stod( line.substr(64,16) );
    particle.m_px = std::stod( line.substr(80,16) );
    particle.m_py = std::stod( line.substr(96,16) );
    particle.m_pz = std::stod( line.substr(112,16) );
    particle.m_mass = std::stod( line.substr(128,16) );
    particle.m_ityp = std::stoi( line.substr(144,11) );
    particle.m_dIz = std::stoi( line.substr(155,3) );
    particle.m_chg = std::stoi( line.substr(158,3) );
    particle.m_lcl = std::stoi( line.substr(161,9) );
    particle.m_Ncol = std::stoi( line.substr(170,5) );

    particle.m_ppr = std::stoi( line.substr(175,4) );
    particle.m_time_fr = std::stod( line.substr(179,16) );
    particle.m_x_fr = std::stod( line.substr(195,16) );
    particle.m_y_fr = std::stod( line.substr(211,16) );
    particle.m_z_fr = std::stod( line.substr(227,16) );
    particle.m_E_fr = std::stod( line.substr(243,16) );
    particle.m_px_fr = std::stod( line.substr(259,16) );
    particle.m_py_fr = std::stod( line.substr(275,16) );
    particle.m_pz_fr = std::stod( line.substr(291,16) );

    return is;
}

//------ URQMD FILE 14 ---
template <>
class urqmd_particle<urqmd_filetype::F14> : public urqmd_basicparticle
{
    friend line_stream& operator>> <urqmd_filetype::F14>(line_stream&, urqmd_particle&);
public:
    class extended {
        friend class urqmd_particle<urqmd_filetype::F14>;
    public:
        double t_decay() const { return m_tdecay; }
        double t_formation() const { return m_tform; }
        double xsection_reduction() const { return m_xsection_red; }
        int unique_i() const { return m_unique_id; }

    private:
        double m_tdecay;
        double m_tform;
        double m_xsection_red;
        int m_unique_id;
    };

    urqmd_particle()
        : m_ppr{0} // parent process type
    { }

    int parent_proc_id() const { return m_ppr; }

    const std::experimental::optional<extended>& extendedInfo() const { return m_extendedInfo; }
private:
    void setExtended(double tdecay, double tformation, double xsec_red, int uniqueId)
    {
        extended exinfo;
        exinfo.m_tdecay = tdecay;
        exinfo.m_tform  = tformation;
        exinfo.m_xsection_red = xsec_red;
        exinfo.m_unique_id = uniqueId;

        // replaces current value with exinfo
        m_extendedInfo = exinfo;
    }

    int m_ppr; // parent process type
    std::experimental::optional<extended> m_extendedInfo;
};

template <>
inline line_stream& operator>>(line_stream& is, urqmd_particle<urqmd_filetype::F14>& particle)
{
    std::string line;

    // parse frame info
    // check a possible good frame header
    if( !(getline(is,line) && (line.length()==179 || line.length()==241)) )
    {
        is.unget();
        is.setstate(std::ios_base::failbit);
        return is;
    }

    // parse base particle info
    particle.m_t = std::stod( line.substr(0,16) );
    particle.m_x = std::stod( line.substr(16,16) );
    particle.m_y = std::stod( line.substr(32,16) );
    particle.m_z = std::stod( line.substr(48,16) );
    particle.m_E = std::stod( line.substr(64,16) );
    particle.m_px = std::stod( line.substr(80,16) );
    particle.m_py = std::stod( line.substr(96,16) );
    particle.m_pz = std::stod( line.substr(112,16) );
    particle.m_mass = std::stod( line.substr(128,16) );
    particle.m_ityp = std::stoi( line.substr(144,11) );
    particle.m_dIz = std::stoi( line.substr(155,3) );
    particle.m_chg = std::stoi( line.substr(158,3) );
    particle.m_lcl = std::stoi( line.substr(161,9) );
    particle.m_Ncol = std::stoi( line.substr(170,5) );



    if(line.length()==241) { // if we have extended info available
        particle.m_ppr = std::stoi( line.substr(175,10) );
        particle.setExtended( std::stod( line.substr(185,16) ),
                              std::stod( line.substr(201,16) ),
                              std::stod( line.substr(217,16) ),
                              std::stoi( line.substr(233,8) ));
    }
    else
        particle.m_ppr = std::stoi( line.substr(175,4) );

    return is;
}

//------ URQMD FILE 15 ---
template <>
class urqmd_particle<urqmd_filetype::F15> : public urqmd_basicparticle
{
    friend line_stream& operator>> <urqmd_filetype::F15>(line_stream&, urqmd_particle&);
public:
    urqmd_particle()
        : m_id{0},
          m_S{0}, // strangeness
          m_history{0} // history information (for debugging)
    { }

    int index() const { return m_id; }
    int strangeness() const { return m_S; }
    long int history() const { return m_history; }
private:
    int m_id;
    int m_S; // strangeness
    long int m_history; // history information (for debugging)
};

template <>
line_stream& operator>>(line_stream& is, urqmd_particle<urqmd_filetype::F15>& particle)
{
    std::string line;

    // parse frame info
    // check a possible good frame header
    if( !(getline(is,line) && line.length()==198) )
    {
        is.unget();
        is.setstate(std::ios_base::failbit);
        return is;
    }

    particle.m_id = std::stoi( line.substr(0,5) );
    particle.m_t = std::stod( line.substr(5,16) );
    particle.m_x = std::stod( line.substr(21,16) );
    particle.m_y = std::stod( line.substr(37,16) );
    particle.m_z = std::stod( line.substr(53,16) );
    particle.m_E = std::stod( line.substr(69,16) );
    particle.m_px = std::stod( line.substr(85,16) );
    particle.m_py = std::stod( line.substr(101,16) );
    particle.m_pz = std::stod( line.substr(117,16) );
    particle.m_mass = std::stod( line.substr(133,16) );
    particle.m_ityp = std::stoi( line.substr(149,11) );
    particle.m_dIz = std::stoi( line.substr(160,3) );
    particle.m_chg = std::stoi( line.substr(163,3) );
    particle.m_lcl = std::stoi( line.substr(166,9) );
    particle.m_Ncol = std::stoul( line.substr(175,5) );

    particle.m_S = std::stoi( line.substr(180,3) );
    particle.m_history = std::stol( line.substr(183,15) );

    return is;
}

//------ URQMD FILE 16 ---
template <>
class urqmd_particle<urqmd_filetype::F16> : public urqmd_basicparticle
{
    friend line_stream& operator>> <urqmd_filetype::F16>(line_stream&, urqmd_particle&);
public:
    class extended {
        friend class urqmd_particle<urqmd_filetype::F16>;
    public:
        int parent_ityp1() const { return m_ityp1; }
        int parent_ityp2() const { return m_ityp2; }
    private:
        int m_ityp1; // ityp-old 1 : particle-ID of parent particle # 1
        int m_ityp2; // ityp-old 1 : particle-ID of parent particle # 1
    };

    urqmd_particle()
        : m_ppr{0} // parent process type
    { }

    int parent_proc_id() const { return m_ppr; }
    const std::experimental::optional<extended>& extendedInfo() const { return m_extendedInfo; }
private:
    void setExtended(int pid1, int pid2)
    {
        extended exinfo;
        exinfo.m_ityp1 = pid1;
        exinfo.m_ityp2 = pid2;

        // replaces current value with exinfo
        m_extendedInfo = exinfo;
    }

    int m_ppr; // parent process type
    std::experimental::optional<extended> m_extendedInfo;

};

template <>
line_stream& operator>>(line_stream& is, urqmd_particle<urqmd_filetype::F16>& particle)
{
    std::string line;

    // parse frame info
    // check a possible good frame header
    if( !(getline(is,line) && (line.length()==179 || line.length()==178) ))
    {
        is.unget();
        is.setstate(std::ios_base::failbit);
        return is;
    }

    // parse base particle info
    if(line.length()==179) // normal file
    {
        particle.m_t = std::stod( line.substr(0,16) );
        particle.m_x = std::stod( line.substr(16,16) );
        particle.m_y = std::stod( line.substr(32,16) );
        particle.m_z = std::stod( line.substr(48,16) );
        particle.m_E = std::stod( line.substr(64,16) );
        particle.m_px = std::stod( line.substr(80,16) );
        particle.m_py = std::stod( line.substr(96,16) );
        particle.m_pz = std::stod( line.substr(112,16) );
        particle.m_mass = std::stod( line.substr(128,16) );
        particle.m_ityp = std::stoi( line.substr(144,11) );
        particle.m_dIz = std::stoi( line.substr(155,3) );
        particle.m_chg = std::stoi( line.substr(158,3) );
        particle.m_lcl = std::stoi( line.substr(161,9) );
        particle.m_Ncol = std::stoi( line.substr(170,5) );
        particle.m_ppr = std::stoi( line.substr(175,4) );
    }
    else if(line.length()==178 ) // extended info available
    {
        particle.m_t = std::stod( line.substr(0,15) );
        particle.m_x = std::stod( line.substr(15,15) );
        particle.m_y = std::stod( line.substr(30,15) );
        particle.m_z = std::stod( line.substr(45,15) );
        particle.m_E = std::stod( line.substr(60,15) );
        particle.m_px = std::stod( line.substr(75,15) );
        particle.m_py = std::stod( line.substr(90,15) );
        particle.m_pz = std::stod( line.substr(105,15) );
        particle.m_mass = std::stod( line.substr(120,15) );
        particle.m_ityp = std::stoi( line.substr(135,11) );
        particle.m_dIz = std::stoi( line.substr(146,3) );
        particle.m_chg = std::stoi( line.substr(149,3) );
        particle.m_lcl = std::stoi( line.substr(152,9) );
        particle.m_Ncol = std::stoi( line.substr(161,5) );
        particle.m_ppr = std::stoi( line.substr(166,4) );
        particle.setExtended(std::stoi( line.substr(170,4)), std::stoi( line.substr(174,4) ));
    }


    return is;
}

#endif // URQMD_PARTICLE_H
