/**
 ** This file is part of the urqmdfc project.
 ** Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef LINE_STREAM_H
#define LINE_STREAM_H

#include <string>
#include <istream>

class line_stream
{
public:
    line_stream(std::istream& is);

    std::string get(); // retrieve a line (from buffer or a fresh one)
    void unget(); // save buffer, next get() will retrieve the buffer
    void putback(std::string line); // makes this line the current buffer

    operator bool() const { return static_cast<bool>(is); }

    void setstate(std::ios_base::iostate s){ is.setstate(s); }
    void clear() { is.clear(); }
private:
    std::istream& is;
    bool isBufferFull;
    std::string buffer;
};

inline line_stream& getline(line_stream& ls, std::string& lineStr)
{
    lineStr = ls.get();
    return ls;
}

#endif // LINE_STREAM_H
