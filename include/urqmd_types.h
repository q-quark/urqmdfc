/**
 ** This file is part of the urqmdfc project.
 ** Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef URQMD_TYPES_H
#define URQMD_TYPES_H

#define APP_SHORT_NAME "urqmdfc"
#define APP_WEB_REPORT_BUGS "http://github.com/"


typedef unsigned int uint;
typedef unsigned long ulong;

enum urqmd_filetype {
    UNKNOWN=0,
    F13=13,
    F14=14,
    F15=15,
    F16=16
};

#endif // URQMD_TYPES_H
