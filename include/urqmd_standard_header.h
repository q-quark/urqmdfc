/**
 ** This file is part of the urqmdfc project.
 ** Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef URQMD_STANDARD_HEADER_H
#define URQMD_STANDARD_HEADER_H

#include <string>
#include <vector>
#include <stdexcept>

#include "line_stream.h"
#include "urqmd_types.h"

template <urqmd_filetype FileType> class urqmd_event_header;

template <urqmd_filetype FileType>
line_stream& operator>>(line_stream&, urqmd_event_header<FileType>&);


template <urqmd_filetype FileType>
class urqmd_event_header
{
    friend line_stream& operator>> <FileType>(line_stream&, urqmd_event_header&);
public:
    typedef std::vector<double>::size_type vd_sz;
    typedef std::vector<uint>::size_type vi_sz;

    urqmd_event_header();

    ulong id() const { return m_id; }

    uint version_min() const { return m_urqmd_version_min; }
    uint version_max() const { return m_urqmd_version_max; }
    uint version() const { return m_urqmd_version; }

    urqmd_filetype type() const { return FileType; }

    uint projectile_A() const { return m_A1; }
    uint projectile_Z() const { return m_Z1; }
    uint target_A() const { return m_A2; }
    uint target_Z() const { return m_Z2; }

    double trans_beta_NN() const { return m_transbNN; }
    double trans_beta_Lab() const { return m_transbLab; }
    double trans_beta_Pro() const { return m_transbPro; }

    double b() const { return m_breal; }
    double b_min() const { return m_bmin; }
    double b_max() const { return m_bmax; }

    double xsection() const { return m_xsection; }
    uint EOS() const { return m_eos; }
    double energy() const { return m_Elab; }
    double ECM() const { return m_s; }
    double momentum() const { return m_Plab; }
    uint time() const { return m_time; }
    double timestep() const { return m_dtime; }

    ulong seed() const { return m_seed; }

    const auto& parameters() const { return m_CTP; }
    const auto& options() const { return m_CTO; }
    vd_sz parameters_count() const { return m_CTP.size(); }
    vi_sz options_count() const { return m_CTO.size(); }

    double parameter(vd_sz i) const { return m_CTP.at(i); }
    uint option(vi_sz i) const { return m_CTO.at(i); }

    void clear();

private:
    void reserveParameters(uint nParams, uint mOptions);

    ulong m_id; // a counter
    uint m_A1;// projectile
    uint m_Z1;
    uint m_A2;// target
    uint m_Z2;
    double m_transbNN; // transformation betas
    double m_transbLab;
    double m_transbPro;
    double m_breal;// real impact parameter
    double m_bmin;// min impact parameter
    double m_bmax;// max impact parameter
    double m_xsection; //cross section
    uint m_eos; // equation of state
    double m_Elab; // energy lab
    double m_s;  // sqrt(s) in lab
    double m_Plab; // momentum lab
    ulong m_seed; // random seed
    uint m_time; // total time
    double m_dtime; // time step

    uint m_urqmd_version_min;
    uint m_urqmd_version_max;
    uint m_urqmd_version;

    std::vector<double> m_CTP; // CTParameters
    std::vector<uint> m_CTO; // CTOptions
};

template <urqmd_filetype FileType>
urqmd_event_header<FileType>::urqmd_event_header()
    :  m_id{0}, // a sql unique id
      m_A1{0},// projectile
      m_Z1{0},
      m_A2{0},// target
      m_Z2{0},
      m_transbNN{0}, // transformation betas
      m_transbLab{0},
      m_transbPro{0},
      m_breal{0},// impact parameter
      m_bmin{0},// impact parameter
      m_bmax{0},// impact parameter
      m_xsection{0}, //cross section
      m_eos{0}, // equation of state
      m_Elab{0}, // energy lab
      m_s{0},  // sqrt(s) in lab
      m_Plab{0}, // momentum lab
      m_seed{0}, // random seed
      m_time{0}, // total time
      m_dtime{0}, // time step
      m_urqmd_version_min{0},
      m_urqmd_version_max{0},
      m_urqmd_version{0}
{ }

template<urqmd_filetype FileType>
void urqmd_event_header<FileType>::clear()
{
    m_CTO.clear();
    m_CTP.clear();
}

template <urqmd_filetype FileType>
void urqmd_event_header<FileType>::reserveParameters(uint nParams, uint mOptions)
{
    m_CTP.reserve(nParams);
    m_CTO.reserve(mOptions);
}

template <urqmd_filetype FileType>
line_stream& operator>>(line_stream& is, urqmd_event_header<FileType>& event_header)
{
    std::string line;

    // ---- parsing line 1
    // check a possible good event header
    if( !(getline(is,line) && line.substr(0,4) == "UQMD" && line.length()==58) ) { is.unget(); is.setstate(std::ios_base::failbit); return is; }
    event_header.m_urqmd_version_max = std::stoi(line.substr(20,7));
    event_header.m_urqmd_version_min = std::stoi(line.substr(27,7));
    event_header.m_urqmd_version = std::stoi(line.substr(34,7));

    if((int)FileType != std::stoi(line.substr(56,2)) ) { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event type"); }

    // ---- parsing line 2
    if( !(getline(is,line) && line.length()==68) ) { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 2"); }

    if(line.substr(0,26) != "projectile:  (mass, char) ") { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 2 - expected projectile masses"); }
    event_header.m_A1 = std::stoi(line.substr(26,4));
    event_header.m_Z1 = std::stoi(line.substr(30,4));

    if(line.substr(34,25) != "   target:  (mass, char) ") { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 2 - expected target masses"); }
    event_header.m_A2 = std::stoi(line.substr(59,4));
    event_header.m_Z2 = std::stoi(line.substr(63,4));

    // ---- parsing line 3
    if( !(getline(is,line) && line.length()==69) ) { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 3"); }

    if(line.substr(0,36) != "transformation betas (NN,lab,pro)   ") { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 3 - expected transformation betas"); }
    event_header.m_transbNN  = std::stod(line.substr(36,11));
    event_header.m_transbLab = std::stod(line.substr(47,11));
    event_header.m_transbPro = std::stod(line.substr(58,11));

    // ---- parsing line 4
    if( !(getline(is,line) && line.length()==94) ) { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 4"); }

    if(line.substr(0,36) != "impact_parameter_real/min/max(fm):  ") { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 4 - expected impact parameter"); }
    event_header.m_breal= std::stod(line.substr(36,6));
    event_header.m_bmin = std::stod(line.substr(42,6));
    event_header.m_bmax = std::stod(line.substr(48,6));

    if(line.substr(54,31) != "  total_cross_section(mbarn):  ") { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 4 - expected total cross section"); }
    event_header.m_xsection= std::stod(line.substr(85,9));

    // ---- parsing line 5
    if( !(getline(is,line) && line.length()==101) ) { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 5"); }

    if(line.substr(0,19) != "equation_of_state: ") { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 5 - expected equation of state"); }
    event_header.m_eos = std::stoi(line.substr(20,3));

    if(line.substr(23,15) != "  E_lab(GeV/u):") { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 5 - expected Energy in Lab"); }
    event_header.m_Elab = std::stod(line.substr(38,11));

    if(line.substr(49,15) != "  sqrt(s)(GeV):") { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 5 - expected sqrt(s)"); }
    event_header.m_s = std::stod(line.substr(64,11));

    if(line.substr(75,15) != "  p_lab(GeV/u):") { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 5 - expected Momentum in Lab"); }
    event_header.m_Plab = std::stod(line.substr(90,11));

    // ---- parsing line 6
    if( !(getline(is,line) && line.length()==108) ) { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 6"); }

    if(line.substr(0,7) != "event# ") { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 6 - expected event id"); }
    event_header.m_id = std::stoul(line.substr(7,9));

    if(line.substr(16,13) != " random seed:") { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 6 - expected random seed"); }
    event_header.m_seed = std::stoul(line.substr(29,12));

    if(line.substr(50,20) != " total_time(fm/c):  ") { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 6 - expected total time"); }
    event_header.m_time = std::stod(line.substr(70,7));

    if(line.substr(77,20) != " Delta(t)_O(fm/c):  ") { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 6 - expected Delta(t)"); }
    event_header.m_dtime = std::stod(line.substr(97,11));

    // ------  parse CTOptions
    event_header.m_CTO.clear();
    if( !(getline(is,line) && line.length()==77 && line.substr(0,2)=="op") )  { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: line 6 - expected CTOptions"); }
    do {
        for(int i=2; i<74; i+=5){
            event_header.m_CTO.push_back( std::stoi(line.substr(i,3)) );
        }
    } while(getline(is,line) && line.length()==77 && line.substr(0,2)=="op");

    if(event_header.m_CTO.empty()) { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: NO CTOptions"); }

    // ------ parse CTParameters
    event_header.m_CTP.clear();
    // previous getline() should contain a line with "CTP"
    if( !(line.length()==158 && line.substr(0,2)=="pa") ) { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: expected CTParameters"); }
    do{
        for(int i=2; i<146; i+=13){
            event_header.m_CTP.push_back( std::stod(line.substr(i,11)) );
        }
    } while(getline(is,line) && line.length()==158 && line.substr(0,2)=="pa");

    if(event_header.m_CTP.empty()) { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: NO CTParameters"); }

    // ------ pvec
    // previous getline() should contain a line with "pvec"
    if( !(line.length()==171 && line.substr(0,4)=="pvec") ) { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid event header: expected pvec line"); }

    return is;
}

//------ URQMD FILE 15 ---
template <>
class urqmd_event_header<urqmd_filetype::F15>
{
    friend line_stream& operator>> <urqmd_filetype::F15>(line_stream&, urqmd_event_header&);
public:
    typedef std::vector<double>::size_type vd_sz;
    typedef std::vector<uint>::size_type vi_sz;

    urqmd_event_header()
        : m_id_event{0},
          m_pro_A{0},
          m_tar_A{0},
          m_b{0},
          m_snn{0},
          m_xsection{0},
          m_Elab{0},
          m_Plab{0}
    { }

    int id() const { return m_id_event; }
    uint projectile_A() const { return m_pro_A; }
    uint target_A() const { return m_tar_A; }
    double b() const { return m_b; }
    double energy() const { return m_Elab; }
    double ECM() const { return m_snn; }
    double momentum() const { return m_Plab; }
    double xsection() const { return m_xsection; }

    vd_sz parameters_count() const =delete; // this is necessary due to call from event
    vi_sz options_count() const =delete; // this is necessary due to call from event
    double parameter(vd_sz i) const =delete; // this is necessary due to call from event
    uint option(vi_sz i) const =delete; // this is necessary due to call from event
    void clear() =delete;
private:
    int m_id_event;
    uint m_pro_A;
    uint m_tar_A;
    float m_b;
    double m_snn;
    double m_xsection;
    double m_Elab;
    double m_Plab;
};

template <>
line_stream& operator>>(line_stream& is, urqmd_event_header<urqmd_filetype::F15>& event_header)
{
    std::string line;

    // check a possible good event header
    if( !(getline(is,line) && line.length()==83 && std::stoi(line.substr(0,8)) == -1) ) { is.unget(); is.setstate(std::ios_base::failbit); return is; }
    event_header.m_id_event = std::stoi(line.substr(8,8));
    event_header.m_pro_A = std::stoi(line.substr(16,4));
    event_header.m_tar_A = std::stoi(line.substr(20,7));
    event_header.m_b = std::stof(line.substr(27,8));
    event_header.m_snn = std::stod(line.substr(35,12));
    event_header.m_xsection = std::stod(line.substr(47,12));
    event_header.m_Elab = std::stod(line.substr(59,12));
    event_header.m_Plab = std::stod(line.substr(71,12));

    return is;
}

#endif // URQMD_STANDARD_HEADER_H
