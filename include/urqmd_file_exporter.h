/**
 ** This file is part of the urqmdfc project.
 ** Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef URQMD_FILE_EXPORTER_H
#define URQMD_FILE_EXPORTER_H

#include <string>

#include "urqmd_types.h"

template <urqmd_filetype FileType> class urqmd_file;
template <urqmd_filetype FileType> class urqmd_event_header;
template <urqmd_filetype FileType> class urqmd_frame;
template <urqmd_filetype FileType> class urqmd_particle;

// the exporter builder interface
template <urqmd_filetype FileType>
class urqmd_base_exporter
{
public:
    virtual ~urqmd_base_exporter() {}

    virtual urqmd_base_exporter<FileType>* clone() =0;
    virtual std::string file_extension() const =0;

    virtual bool open(const std::string& filename) =0;
    virtual void close() =0;

    virtual void begin_event(uint event_id)=0; // gets called *before* an event header is going to be written
    virtual void end_event()=0; // gets called *after* all frames and all particles from an event were written

    virtual void begin_frame(uint frame_id)=0; // gets called *before* a frame is going to be written
    virtual void end_frame()=0; // gets called *after* all particles in a frame were written and the frame has finished

    virtual void write_header(const urqmd_event_header<FileType>& header) =0;
    virtual void write_frame(uint event_id, const urqmd_frame<FileType>& frame) =0;
    virtual void write_particle(uint event_id, uint frame_id, const urqmd_particle<FileType>& particle) =0;
};


/*
 *   urqmd_file_export_director an exporter from in-memory data of a urqmd_file
 *
 *  for example:
 *      hdf5_exporter<FileType> hdf5Exporter;
 *
 *      urqmd_file_export_director<FileType> director(file);
 *      director.setExporter(&hdf5Exporter);
 *      director.exportTo("exported.hdf");
 *
 *
 *
*/


template <urqmd_filetype FileType>
class urqmd_file_export_director
{
public:
    urqmd_file_export_director(urqmd_file<FileType>& urqmdfile)
        : file{urqmdfile}, exp{nullptr} { }

    void setExporter(urqmd_base_exporter<FileType>* exporter) { exp = exporter; }
    void exportTo(const std::string& filename);

private:
    urqmd_file<FileType>& file;
    urqmd_base_exporter<FileType>* exp;
};

template<urqmd_filetype FileType>
void urqmd_file_export_director<FileType>::exportTo(const std::string &filename)
{
    if(!exp || !exp->open(filename)) throw std::runtime_error("no valid file to export to.");

    for(typename urqmd_file<FileType>::events_sz ievent=0; ievent<file.count_events(); ++ievent){
        exp->begin_event(ievent);
        exp->write_header(file.get_event_header(ievent));

        for(typename urqmd_file<FileType>::frames_sz iframe=0; iframe<file.count_frames(ievent); ++iframe){
            exp->begin_frame(iframe);
            exp->write_frame(ievent, file.get_frame(ievent,iframe));

            for(typename urqmd_file<FileType>::particles_sz iparticle=0; iparticle<file.count_particles(ievent, iframe); ++iparticle){
                exp->write_particle(ievent, iframe, file.get_particle(ievent,iframe,iparticle));
            }
            exp->end_frame();
        }
        exp->end_event();
    }

    exp->close();
}


#endif // URQMD_FILE_EXPORTER_H
