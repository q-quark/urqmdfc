/**
 ** This file is part of the urqmdfc project.
 ** Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <ostream>
#include <iomanip>
#include <algorithm>
#include <iterator>

template <typename T>
class format {
public:
    format(short field_width, T value)
        : w{field_width}, prec{0}, v{value}
    { }

    format(short field_width, short precision, T value)
        : w{field_width}, prec{precision}, v{value}
    { }

    std::ostream &operator()(std::ostream& os) const
    {
        os << std::setw(w) << std::fixed << std::setprecision(prec) << v;

        return os;
    }

private:
    short w;
    short prec;
    T v;
};

template <typename T>
std::ostream& operator<<(std::ostream& os,const format<T>& f) { return f(os); }

template <typename T>
class sci_format {
public:
    sci_format(short field_width, T value)
        : w{field_width}, prec{0}, v{value}
    { }

    sci_format(short field_width, short precision, T value)
        : w{field_width}, prec{precision}, v{value}
    { }

    std::ostream &operator()(std::ostream& os) const
    {
        os << std::setw(w) << std::scientific << std::setprecision(prec) << v;

        return os;
    }

private:
    short w;
    short prec;
    T v;
};

template <typename T>
std::ostream& operator<<(std::ostream& os,const sci_format<T>& f) { return f(os); }


// returns the file path contained in 'path' removing the file extension
std::string remove_file_extension(const std::string& path)
{
    auto iter = std::find(path.rbegin(), path.rend(), '.');

    return iter == path.rend() ? path : std::string(path.begin(), iter.base());
}
#endif // UTILS_H
