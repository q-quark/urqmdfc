/**
 ** This file is part of the urqmdfc project.
 ** Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef URQMD_FILE_H
#define URQMD_FILE_H

#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <memory>

#include <cctype>
#include <cstring>

#include "line_stream.h"

#include "urqmd_types.h"
#include "urqmd_standard_header.h"
#include "urqmd_frame.h"
#include "urqmd_particle.h"

#include "urqmd_file_exporter.h"
#include "exporters/urqmd_exporter.h"
#include "exporters/hdf5_exporter.h"

class urqmd_base_file
{
public: // user-interface
    // return a file handle to a URQMD File of type F13, F14, F15, F16
    // returns an empty pointer if the file is not a valid URQMD file or if it couldn't read the file
    static std::unique_ptr<urqmd_base_file> get(const std::string& filename);

    virtual ~urqmd_base_file(){}

    virtual void open(const std::string& filename) =0;
    virtual void close() =0;


    virtual urqmd_filetype filetype() const =0;

    virtual void readAll() =0;

    virtual void exportAs(const std::string& filename, const std::string& type) =0;

    virtual bool is_valid() const =0;
    virtual operator bool() const =0;

    bool isVerboseOutput{false};
};


template <urqmd_filetype FileType>
class urqmd_file : public urqmd_base_file
{
public:
    typedef typename std::vector< urqmd_event_header<FileType> >::size_type events_sz;
    typedef typename std::vector<urqmd_frame<FileType> >::size_type frames_sz;
    typedef typename std::vector<urqmd_particle<FileType>>::size_type particles_sz;

    urqmd_file() { }
    urqmd_file(const std::string& fileName)
    {
        open(fileName);
    }

    ~urqmd_file(){}

    urqmd_file(const urqmd_file<FileType>& other);
    urqmd_file(urqmd_file<FileType>&& other);

    urqmd_file<FileType>& operator=(const urqmd_file<FileType>& other);
    urqmd_file<FileType>& operator=(urqmd_file<FileType>&& other);

    urqmd_filetype filetype() const override { return FileType; }

    void open(const std::string& fileName)  override;
    void close()  override {}
    void readAll() override;

    // these members trow if out_of_bounds
    events_sz count_events() const;
    frames_sz count_frames(events_sz ievent) const;
    particles_sz count_particles(events_sz ievent, frames_sz iframe) const;

    // these members trow if out_of_bounds
    const urqmd_event_header<FileType>& get_event_header(events_sz i) const;
    const urqmd_frame<FileType>& get_frame(events_sz ievent, frames_sz iframe) const; // get frame 'iframe' from event number 'ievent'
    const urqmd_particle<FileType>& get_particle(events_sz ievent, frames_sz iframe, particles_sz iparticle) const; // get particle with record number 'iparticle' from frame 'iframe' that is inside event 'ievent'

    void exportAs(const std::string& filename, const std::string& type) override;

    bool is_valid() const  override { return (*this); }
    operator bool() const  override { return static_cast<bool>(m_fileH); }

private:
    void parse_to_mem(); // parse current opened file into memory
    void parse_and_export_to(const std::string& filename);

    std::string m_fileName;
    std::ifstream m_fileH; // file handler
    std::unique_ptr<urqmd_base_exporter<FileType>> m_fileExporter; // a handler for exporting

    // maps event-id to a header
    std::vector< urqmd_event_header<FileType> > m_eventHeaders;

    // maps event-id to a list of frames
    std::map< events_sz, std::vector<urqmd_frame<FileType>>> m_frames;

    // // maps event-id to a map of particles ( maps frame-id to a list of particles)
    std::map< events_sz, std::map< frames_sz, std::vector<urqmd_particle<FileType>>>> m_particles;
};


template<urqmd_filetype FileType>
urqmd_file<FileType>::urqmd_file(const urqmd_file<FileType> &other)
    : m_fileName{other.m_fileName},
      m_fileH{other.m_fileH}
{
    m_eventHeaders = other.m_eventHeaders;
    m_frames = other.m_frames;
    m_particles = other.m_particles;

    m_fileExporter.reset(other.m_fileExporter->clone());
}

template<urqmd_filetype FileType>
urqmd_file<FileType>::urqmd_file(urqmd_file<FileType> &&other)
    : m_fileName{std::move(other.m_fileName)},
      m_fileH{other.m_fileH}
{
    m_eventHeaders = std::move(other.m_eventHeaders);
    m_frames = std::move(other.m_frames);
    m_particles = std::move(other.m_particles);

    other.m_fileName.clear();
    other.m_fileH.close();

    m_fileExporter = other.m_fileExporter;
}

template<urqmd_filetype FileType>
urqmd_file<FileType>& urqmd_file<FileType>::operator=(const urqmd_file<FileType> &other)
{
    if(&other!=this){
        m_fileName = other.m_fileName;
        m_fileH = other.m_fileH;
        m_eventHeaders = other.m_eventHeaders;
        m_frames = other.m_frames;
        m_particles = other.m_particles;

        m_fileExporter.reset(other.m_fileExporter->clone());
    }

    return *this;
}

template<urqmd_filetype FileType>
urqmd_file<FileType>& urqmd_file<FileType>::operator=(urqmd_file<FileType>&& other)
{
    if(&other!=this){
        m_fileName = std::move(other.m_fileName);
        m_fileH = other.m_fileH;
        m_eventHeaders = std::move(other.m_eventHeaders);
        m_frames = std::move(other.m_frames);
        m_particles = std::move(other.m_particles);

        other.m_fileH.close();

        m_fileExporter = other.m_fileExporter;
    }

    return *this;
}

template <urqmd_filetype FileType>
void urqmd_file<FileType>::open(const std::string &fileName)
{
    m_fileName = fileName;
    m_fileH.close();

    m_fileH.open(fileName);
}

template<urqmd_filetype FileType>
void urqmd_file<FileType>::readAll()
{
    if(!(*this)) throw std::runtime_error("Can not read file - No URQMD file is opened.");

    parse_to_mem();
}

template<urqmd_filetype FileType>
typename urqmd_file<FileType>::events_sz
urqmd_file<FileType>::count_events() const
{
    return m_eventHeaders.size();
}

template<urqmd_filetype FileType>
typename urqmd_file<FileType>::frames_sz
urqmd_file<FileType>::count_frames(typename urqmd_file<FileType>::events_sz ievent) const
{
    return m_frames.at(ievent).size();
}

template<urqmd_filetype FileType>
typename urqmd_file<FileType>::particles_sz
urqmd_file<FileType>::count_particles(typename urqmd_file<FileType>::events_sz ievent, typename urqmd_file<FileType>::frames_sz iframe) const
{
    return m_particles.at(ievent).at(iframe).size();
}

template<urqmd_filetype FileType>
const urqmd_event_header<FileType> &urqmd_file<FileType>::get_event_header(typename urqmd_file<FileType>::events_sz i) const
{
    return m_eventHeaders[i];
}

template<urqmd_filetype FileType>
const urqmd_frame<FileType> &urqmd_file<FileType>::get_frame(typename urqmd_file<FileType>::events_sz ievent, typename urqmd_file<FileType>::frames_sz iframe) const
{
    return m_frames.at(ievent).at(iframe);
}

template<urqmd_filetype FileType>
const urqmd_particle<FileType> &urqmd_file<FileType>::get_particle(typename urqmd_file<FileType>::events_sz ievent, typename urqmd_file<FileType>::frames_sz iframe, typename urqmd_file<FileType>::particles_sz iparticle) const
{
    return m_particles.at(ievent).at(iframe).at(iparticle);
}


// for F14 & F15
template<urqmd_filetype FileType>
void urqmd_file<FileType>::parse_to_mem()
{
    line_stream is(m_fileH);

    m_eventHeaders.clear();
    m_frames.clear();
    m_particles.clear();

    urqmd_event_header<FileType> event_header;
    urqmd_frame<FileType> readFrame;
    urqmd_particle<FileType> particle;

    uint gIdEvent=0;
    uint gIdFrame=0;
    uint gIdParticle=0;
    for(uint idEvent=0; is; ++idEvent, ++gIdEvent){
        if(!(is>>event_header)) break; // just return if "is" has failbit
        m_eventHeaders.push_back(event_header);

        for(uint idFrame=0; is; ++idFrame, ++gIdFrame){

            if(!(is>>readFrame)) break;
            m_frames[idEvent].push_back(readFrame);

            for(uint idParticle=0; is; ++idParticle, ++gIdParticle){
                if(!(is>>particle)) break;
                m_particles[idEvent][idFrame].push_back(particle);
            }
            is.clear();
        }
        is.clear();
    }

    //    std::cout << "Parsed: " << gIdEvent << " events  \t" << gIdFrame << " frames \t" << gIdParticle<<" particles "<< std::endl;
}

// for F13
template<>
void urqmd_file<urqmd_filetype::F13>::parse_to_mem()
{
    line_stream is(m_fileH);

    m_eventHeaders.clear();
    m_frames.clear();
    m_particles.clear();

    urqmd_event_header<urqmd_filetype::F13> event_header;
    urqmd_frame<urqmd_filetype::F13> readFrame;
    urqmd_particle<urqmd_filetype::F13> particle;

    for(uint idEvent=0; is; ++idEvent){
        event_header.clear();
        if(!(is>>event_header)) break; // just return if "is" has failbit
        m_eventHeaders.push_back(event_header);

        if(!(is>>readFrame)) break;
        m_frames[idEvent].push_back(readFrame);

        for(uint idParticle=0; idParticle<readFrame.particles() && is; ++idParticle){
            if(!(is>>particle)) break;
            m_particles[idEvent][0].push_back(particle);
        }
    }
}

// for F16
template<>
void urqmd_file<urqmd_filetype::F16>::parse_to_mem()
{
    line_stream is(m_fileH);

    m_eventHeaders.clear();
    m_frames.clear();
    m_particles.clear();

    urqmd_event_header<urqmd_filetype::F16> event_header;
    urqmd_frame<urqmd_filetype::F16> readFrame;
    urqmd_particle<urqmd_filetype::F16> particle;

    for(uint idEvent=0; is; ++idEvent){
        event_header.clear();
        if(!(is>>event_header)) break; // just return if "is" has failbit
        m_eventHeaders.push_back(event_header);

        for(uint idParticle=0; is; ++idParticle){
            if(!(is>>particle)) break;
            m_particles[idEvent][0].push_back(particle);
        }

        is.clear();

        if(!(is>>readFrame)) throw std::runtime_error("invalid frame: expected F16 frame");
        m_frames[idEvent].push_back(readFrame);
    }
}

template<urqmd_filetype FileType>
void urqmd_file<FileType>::parse_and_export_to(const std::string &filename)
{
    if(!m_fileExporter || !m_fileExporter->open(filename)) throw std::runtime_error("no valid file to export to.");

    line_stream is(m_fileH);

    urqmd_event_header<FileType> event_header;
    urqmd_frame<FileType> readFrame;
    urqmd_particle<FileType> particle;

    uint gIdEvent=0;
    uint gIdFrame=0;
    uint gIdParticle=0;
    for(uint idEvent=0; is; ++idEvent, ++gIdEvent){
        if(!(is>>event_header)) break; // just return if "is" has failbit

        if(isVerboseOutput) std::cout << "processing event:" << idEvent+1 << std::endl;

        m_fileExporter->begin_event(idEvent);
        m_fileExporter->write_header(event_header);

        for(uint idFrame=0; is; ++idFrame, ++gIdFrame){
            if(!(is>>readFrame)) break;

            if(isVerboseOutput) std::cout << "\tframe:" << idFrame+1 << std::endl;
            m_fileExporter->begin_frame(idFrame);
            m_fileExporter->write_frame(idEvent, readFrame);

            for(uint idParticle=0; is; ++idParticle, ++gIdParticle){
                if(!(is>>particle)) break;
                m_fileExporter->write_particle(idEvent, idFrame, particle);
            } // for particles
            m_fileExporter->end_frame();
            is.clear();
        } // for frames
        m_fileExporter->end_event();
        is.clear();
    } // for events

    m_fileExporter->close();
}

template<>
void urqmd_file<urqmd_filetype::F13>::parse_and_export_to(const std::string &filename)
{
    if(!m_fileExporter || !m_fileExporter->open(filename)) throw std::runtime_error("no valid file to export to.");

    line_stream is(m_fileH);

    urqmd_event_header<urqmd_filetype::F13> event_header;
    urqmd_frame<urqmd_filetype::F13> readFrame;
    urqmd_particle<urqmd_filetype::F13> particle;

    for(uint idEvent=0; is; ++idEvent){
        event_header.clear();
        if(!(is>>event_header)) break; // just return if "is" has failbit

        if(isVerboseOutput) std::cout << "processing event:" << idEvent+1 << std::endl;
        m_fileExporter->begin_event(idEvent);
        m_fileExporter->write_header(event_header);

        if(!(is>>readFrame)) break;
        m_fileExporter->begin_frame(0);
        m_fileExporter->write_frame(idEvent, readFrame);

        for(uint idParticle=0; idParticle<readFrame.particles() && is; ++idParticle){
            if(!(is>>particle)) break;
            m_fileExporter->write_particle(idEvent, 0, particle);
        }
        m_fileExporter->end_frame();
        m_fileExporter->end_event();
    }
    m_fileExporter->close();
}

template<>
void urqmd_file<urqmd_filetype::F16>::parse_and_export_to(const std::string &filename)
{
    if(!m_fileExporter || !m_fileExporter->open(filename)) throw std::runtime_error("no valid file to export to.");

    line_stream is(m_fileH);

    urqmd_event_header<urqmd_filetype::F16> event_header;
    urqmd_frame<urqmd_filetype::F16> readFrame;
    urqmd_particle<urqmd_filetype::F16> particle;

    for(uint idEvent=0; is; ++idEvent){
        event_header.clear();
        if(!(is>>event_header)) break; // just return if "is" has failbit
        if(isVerboseOutput) std::cout << "processing event:" << idEvent+1 << std::endl;
        m_fileExporter->begin_event(idEvent);
        m_fileExporter->write_header(event_header);

        m_fileExporter->begin_frame(0);
        for(uint idParticle=0; is; ++idParticle){
            if(!(is>>particle)) break;
            m_fileExporter->write_particle(idEvent, 0, particle);
        }
        is.clear();

        if(!(is>>readFrame)) throw std::runtime_error("invalid frame: expected F16 frame");
        m_fileExporter->write_frame(idEvent, readFrame);
        m_fileExporter->end_frame();
        m_fileExporter->end_event();
    }
    m_fileExporter->close();
}

template<urqmd_filetype FileType>
void urqmd_file<FileType>::exportAs(const std::string &filename, const std::string &type)
{
    if(type=="hdf"){
        m_fileExporter.reset(new hdf5_exporter<FileType>());
    }
    else if(type=="urqmd"){
        m_fileExporter.reset(new urqmd_exporter<FileType>());
    }
    else {
        m_fileExporter.reset();
        return;
    }

    std::string path = remove_file_extension(filename) + "." + m_fileExporter->file_extension();

    if(isVerboseOutput) std::cout << "converting file: " << m_fileName << " to: " << path << std::endl;

    parse_and_export_to(path);
}

// return a file handle to a URQMD File of type F13, F14, F15, F16
// returns an empty pointer if the file is not a valid URQMD file or if it couldn't read the file
std::unique_ptr<urqmd_base_file> urqmd_base_file::get(const std::string &filename)
{
    std::ifstream is{filename};
    std::unique_ptr<urqmd_base_file> result;

    if(!is) return result;

    char c;
    c = is.peek();
    if(c=='U'){ // we might have a F13, F14 or F16 file
        // lets see if we have a valid 1-st line in the header
        char buf[64];
        is.get(buf, 64, '\n');

        if(!is || strlen(buf)!=58) return result;

        std::string bufStr{buf};
        int file_type = std::stoi(bufStr.substr(56,2));
        if(!(bufStr.substr(0,4)=="UQMD" && (file_type==13 || file_type==14 || file_type==16)) ) return result;

        // by all accounts we now have a valid file header
        switch (file_type) {
        case 13:
            result = std::make_unique<urqmd_file<urqmd_filetype::F13>>(filename);
            break;
        case 14:
            result = std::make_unique<urqmd_file<urqmd_filetype::F14>>(filename);
            break;
        case 16:
            result = std::make_unique<urqmd_file<urqmd_filetype::F16>>(filename);
            break;
        default:
            break;
        }

    }
    else { // we might have a F15
        // lets see if we have a valid 1-st line in the header
        char buf[128];
        is.get(buf, 128, '\n');

        if(!is || strlen(buf)!=83) return result;

        std::string bufStr{buf};

        if(std::stoi(bufStr.substr(0,8))!=-1) return result;

        // by all accounts we now have a valid F15 file header
        result = std::make_unique<urqmd_file<urqmd_filetype::F15>>(filename);
    }

    return result;
}


#endif // URQMD_FILE_H
