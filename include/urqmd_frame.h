/**
 ** This file is part of the urqmdfc project.
 ** Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef URQMD_FRAME_H
#define URQMD_FRAME_H

#include <string>
#include <vector>

#include "line_stream.h"
#include "utils.h"

#include "urqmd_types.h"
#include "urqmd_particle.h"

template<urqmd_filetype FileType> class urqmd_frame;

template <urqmd_filetype FileType>
line_stream& operator>>(line_stream &, urqmd_frame<FileType>& );

template <urqmd_filetype FileType>
class urqmd_frame {
    friend line_stream& operator>> <FileType>(line_stream &, urqmd_frame& );
public:
    urqmd_frame()
        : m_id(0), // a counter
          m_Ncoll(0), // # of collisions
          m_Necoll(0), // # of  elastic collisions
          m_Nicoll(0), // # of inelastic collisions
          m_Ndecays(0), // # of decays
          m_Npauli(0), // # of Pauli-blocked collisions
          m_Nhard(0), // # of produced hard baryon resonances
          m_Nsoft(0), // # of produced soft baryon resonances
          m_Nbar(0), // # of baryon resonances produced via a decay of another resonance
          m_Npart(0),
          m_time(0)
    { }

    ulong id() const { return m_id; }
    uint collisions() const { return m_Ncoll; }
    uint collisions_elastic() const { return m_Necoll; }
    uint collisions_inelastic() const { return m_Nicoll; }
    uint collisions_pauli() const { return m_Npauli; }
    uint decays() const { return m_Ndecays; }
    uint resonances_hard() const { return m_Nhard; }
    uint resonances_soft() const { return m_Nsoft; }
    uint resonances_other() const { return m_Nbar; }
    uint particles() const { return m_Npart; }
    double time() const { return m_time; }

private:
    ulong m_id; // a counter
    uint m_Ncoll; // # of collisions
    uint m_Necoll; // # of  elastic collisions
    uint m_Nicoll; // # of inelastic collisions
    uint m_Ndecays; // # of decays
    uint m_Npauli; // # of Pauli-blocked collisions
    uint m_Nhard; // # of produced hard baryon resonances
    uint m_Nsoft; // # of produced soft baryon resonances
    uint m_Nbar; // # of baryon resonances produced via a decay of another resonance
    uint m_Npart;
    double m_time;
};

// pay attention: this operator is for F13 and F14, below are specialized forms for F15 and F16
template <urqmd_filetype FileType>
line_stream& operator>>(line_stream& is, urqmd_frame<FileType>& frame)
{
    std::string line;

    // parse frame info
    // check a possible good frame header
    if( !(getline(is,line) && line.length()==24) )
    {
        is.unget();
        is.setstate(std::ios_base::failbit);
        return is;
    }

    frame.m_Npart= std::stoi(line.substr(0,12));
    frame.m_time = std::stod(line.substr(12,12));

    // parse collision header
    if( !(getline(is,line) && line.length()==64) ) { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid frame header: expected collision header"); }
    frame.m_id=0;
    frame.m_Ncoll = std::stoi(line.substr(0,8));
    frame.m_Necoll = std::stoi(line.substr(8,8));
    frame.m_Nicoll = std::stoi(line.substr(16,8));
    frame.m_Ndecays = std::stoi(line.substr(24,8));
    frame.m_Npauli = std::stoi(line.substr(32,8));
    frame.m_Nhard = std::stoi(line.substr(40,8));
    frame.m_Nsoft = std::stoi(line.substr(48,8));
    frame.m_Nbar = std::stoi(line.substr(56,8));

    return is;
}

template <>
class urqmd_frame<urqmd_filetype::F15>
{
    friend line_stream& operator>> <urqmd_filetype::F15>(line_stream &, urqmd_frame& );
public:

    urqmd_frame()
        : m_nIn{0},
          m_nOut{0},
          m_pId{0},
          m_colls{0},
          m_tcoll{0},
          m_s{0},
          m_xsection{0},
          m_xsection_part{0},
          m_density{0}
    { }

    uint particles() const { return m_nIn+m_nOut; }
    uint particles_in() const  { return m_nIn; }
    uint particles_out() const { return m_nOut; }
    uint processId() const { return m_pId; }
    uint collisions() const { return m_colls; }
    float collision_time() const { return m_tcoll; }
    double ECM() const { return m_s; }
    double xsection() const { return m_xsection; }
    double xsection_partial() const { return m_xsection_part; }
    double density() const { return m_density; }

private:
    uint m_nIn;
    uint m_nOut;
    uint m_pId;
    uint m_colls;
    float m_tcoll;
    double m_s;
    double m_xsection;
    double m_xsection_part;
    double m_density;
};


template <>
line_stream& operator>>(line_stream& is, urqmd_frame<urqmd_filetype::F15>& frame)
{
    std::string line;

    // check a possible good event header
    if( !(getline(is,line) && line.length()==83 && (std::stoi(line.substr(0,8)) > 0)) )
    {
        is.unget();
        is.setstate(std::ios_base::failbit);
        return is;
    }

    frame.m_nIn = std::stoi(line.substr(0,8));
    frame.m_nOut = std::stoi(line.substr(8,8));
    frame.m_pId = std::stoi(line.substr(16,4));
    frame.m_colls = std::stoi(line.substr(20,7));
    frame.m_tcoll = std::stof(line.substr(27,8));
    frame.m_s = std::stod(line.substr(35,12));
    frame.m_xsection = std::stod(line.substr(47,12));
    frame.m_xsection_part = std::stod(line.substr(59,12));
    frame.m_density = std::stod(line.substr(71,12));

    return is;
}

template <>
line_stream& operator>>(line_stream& is, urqmd_frame<urqmd_filetype::F16>& frame)
{
    // now we expect the collisions header
    std::string line;
    // parse collisions info line
    if( !(getline(is,line) && line.length()==65) ) { is.setstate(std::ios_base::failbit); throw std::runtime_error("invalid collisions header: expected an F16 record"); }
    frame.m_id=0;
    frame.m_Ncoll = std::stoi(line.substr(1,8));
    frame.m_Necoll = std::stoi(line.substr(9,8));
    frame.m_Nicoll = std::stoi(line.substr(17,8));
    frame.m_Ndecays = std::stoi(line.substr(25,8));
    frame.m_Npauli = std::stoi(line.substr(33,8));
    frame.m_Nhard = std::stoi(line.substr(41,8));
    frame.m_Nsoft = std::stoi(line.substr(49,8));
    frame.m_Nbar = std::stoi(line.substr(57,8));

    return is;
}

#endif // URQMD_FRAME_H
