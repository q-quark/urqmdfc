/**
 ** This file is part of the urqmdfc project.
 ** Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "line_stream.h"

line_stream::line_stream(std::istream& stream)
    : is{stream},
      isBufferFull{false}
{

}

std::string line_stream::get()
{
    if(!isBufferFull){
        std::getline(is,buffer); // get a fresh new line
    }

    // user must call unget() if it wants the line from buffer
    isBufferFull = false; // .. otherwise, next get() takes a fresh new line

    return buffer;
}

void line_stream::unget()
{
    isBufferFull = true;
}

void line_stream::putback(std::string line)
{
    buffer = line;
    isBufferFull = true;
}


