# urqmdfc
A Convertor for UrQMD generated files (F13,F14,F15,F16)

## Introduction
This application was made to ***convert the text files (ASCII) generated by the UrQMD simulation code to other formats***.
Currently it ***supports HDF5*** files and UrQMD text files (not useful but good for debugging). Other file formats, such 
as SQLite, might be supported in the future.

### About
This project has its roots in my previous projects [f14toROOT](https://sourceforge.net/projects/f14toroot/) and 
[f14tosqlite](https://sourceforge.net/projects/f14tosqlite/). 

### Supported Formats
All file formats generated by UrQMD 3.4 (do not know for UrQMD < 3.4 ): F13, F14, F15, F16 with the following exceptions:
- does not supports files generated with ***box options***
- does not support ***F15 having standard event header*** (if simulation contained CTOption(58) = 1)

## How to build/compile from source
This project was developed and tested on macOS High Sierra, but it should run on any system(Linux, Windows,etc) that meets standard c++17 and the below
prerequisites.

### Prerequisites

What things you need to build the software:

```
- a compiler that supports C++ 17
- cmake >= 2.8
- Boost program_options >= 1.56 
- HDF5 C++ libraries
```

### Building from Source
Once you got the prerequisites, using the terminal, go to the location where you downloaded urqmdfc:
```
cd /path/to/urqmdfc-source-folder/
```
Create a directory to do the building and enter it:
```
cd ..
mkdir build-urqmdfc
cd build-urqmdfc
```
From this location run CMake:
```
cmake ../urqmdfc-source-folder
```
CMake should prepared files for you and if everything is Ok you can go start the building (N is the number of cores of your machine):
```
make -jN
```
If all went Ok you should have an executable named ***urqmfc***

## How to use
The converter is called ***urqmdfc*** and more options can be display with the command:
```
urqmdfc -h
```
### Some examples
The order of the arguments/options is not important

- Converts sim.f13 to converted.h5
```
urqmdfc sim.f13
```
- Converts sim.f13 to mysim.h5
```
urqmdfc sim.f13 -o mysim
```
- Converts mysim.f14 to test.h5
```
urqmdfc -i mysim.f14 -o test
```
- Converts test.f14 to mysim.h5 and show verbose information
```
urqmdfc -i test.f14 -o mysim.hdf -v
```

## Authors

* **Mihai Niculescu** - i.mihai.niculescu@gmail.com

## License

This project is licensed under the GPL License - see the [License.txt](License.txt) file for details
