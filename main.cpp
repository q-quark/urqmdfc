/**
 ** This file is part of the urqmdfc project.
 ** Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include <vector>
#include <string>
#include <iostream>
#include <exception>

#include <boost/program_options.hpp>

#include "urqmd_file.h"

namespace po = boost::program_options;

po::options_description desc("OPTIONS");
po::positional_options_description pos_options;
po::variables_map vm;

void printCopyright()
{
    std::cout << "Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>" << std::endl;
}

void printHelp()
{
    std::cout << "Converts UrQMD generated files (F13,F14,F15,F16) to HDF5\n";
    std::cout << "\n";
    std::cout << "USAGE: " APP_SHORT_NAME " [-input|i] inputfile [-output|o] outputfile [-type|t] (hdf|urqmd) [-verbose|v]\n\n";

    std::cout << desc;

    std::cout << "\nEXAMPLES:\n";
    std::cout << "-Converts 'test.f13' to hdf5: \n\t" APP_SHORT_NAME << " test.f13 \n";
    std::cout << "-Converts 'test.f14' to hdf5 using arguments: \n\t" APP_SHORT_NAME << " -i test.f14 \n";
    std::cout << "-Converts 'test.f13' to hdf5 specifying output type: \n\t" APP_SHORT_NAME << " test.f13 -t hdf\n";
    std::cout << "-Converts 'test.f14' to urqmd specifying output type (good for testing): \n\t" APP_SHORT_NAME << " test.f13 -t urqmd\n";
    std::cout << "-Full agruments example: \n\t" APP_SHORT_NAME << " -i test.f13 -o mysimulation.h5 -t hdf\n";
    std::cout << "\n*** When using options the order is not important: \n\t" APP_SHORT_NAME << " -t hdf -o mysimulation.h5 -i test.f14 \n";

    std::cout << "\nReport bugs to " APP_WEB_REPORT_BUGS << std::endl;
}


int main(int argc, char** argv)
{
    // Declare the supported options.
    try {
        desc.add_options()
                ("help,h", "produce this help message")
                ("input,i", po::value< std::string >(), "input file name")
                ("output,o", po::value< std::string >()->default_value("converted"), "output file name")
                ("type,t", po::value< std::string >()->default_value("hdf"), "output file type\n possible values: (hdf,urqmd)")
                ("verbose,v", "Verbose mode or display progress information")
                ;


        pos_options.add("input", -1);


        po::store(po::command_line_parser(argc, argv).
                  options(desc).positional(pos_options).run(), vm);
        po::notify(vm);

        if (vm.count("help")) {
            printHelp();
            return 1;
        }

        if (!vm.count("input")) throw std::runtime_error("need an input file");

        if (vm.count("type")) {
            std::string t  = vm["type"].as< std::string >();
            if(t!="hdf" && t!="urmqd" ) throw std::runtime_error("invalid output type");
        }

        std::string inputFileName   = vm["input"].as< std::string >();
        std::string outputFileName  = vm["output"].as< std::string >();
        std::string exportType  = vm["type"].as< std::string >();

        auto file = urqmd_base_file::get(inputFileName);

        if(!file) throw std::runtime_error("can not open file");

        if(vm.count("verbose")) file->isVerboseOutput = true;
        file->exportAs(outputFileName,exportType);

    }
    catch(po::required_option& e)
    {
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;

        return 1;
    }
    catch(po::error& e)
    {
        std::cerr << "ERROR: " << e.what() << std::endl << std::endl;

        return 1;
    }
    catch(std::exception& e)
    {
        std::cout << "ERROR: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}
