/**
 ** This file is part of the urqmdfc project.
 ** Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef HDF5_EXPORTER_H
#define HDF5_EXPORTER_H

#include <string>

#include "urqmd_types.h"
#include "urqmd_file_exporter.h"
#include "urqmd_standard_header.h"
#include "urqmd_frame.h"
#include "urqmd_particle.h"

#include "exporters/hdf5_export_types.h"

#include "H5Cpp.h"

template <urqmd_filetype FileType>
class hdf5_exporter : public urqmd_base_exporter<FileType>
{
public:
    using events_buffer_size_t = typename std::vector<hdf_event_header_t<FileType>>::size_type;
    using frames_buffer_size_t = typename std::vector<hdf_frame_t<FileType>>::size_type;
    using particles_buffer_size_t = typename std::vector<hdf_particle_t<FileType>>::size_type;

    hdf5_exporter()
        : m_file{nullptr}
    {
        create_events_datatype();
        create_frames_datatype();
        create_particles_datatype();
    }

    ~hdf5_exporter(){ close(); }

    urqmd_base_exporter<FileType>* clone() override { return new hdf5_exporter<FileType>(); }
    std::string file_extension() const override { return "h5"; }

    bool open(const std::string &filename) override;
    void close() override;

    void begin_event(uint event_id) override;
    void end_event() override;

    void begin_frame(uint frame_id) override;
    void end_frame() override;

    void write_header(const urqmd_event_header<FileType>& header) override;
    void write_frame(uint event_id, const urqmd_frame<FileType>& frame) override;
    void write_particle(uint event_id, uint frame_id, const urqmd_particle<FileType>& particle) override;

    // NO Copy and No Move
    hdf5_exporter(const hdf5_exporter<FileType>&) =delete;
    hdf5_exporter(hdf5_exporter<FileType>&&) =delete;
    hdf5_exporter<FileType>& operator =(const hdf5_exporter<FileType>&) =delete;
    hdf5_exporter<FileType>& operator =(hdf5_exporter<FileType>&&) =delete;

    // maximum size of the events buffer before it forces a flush
    events_buffer_size_t flush_events_maxbuffer{100};
    // maximum size of the frames buffer before it forces a flush
    frames_buffer_size_t flush_frames_maxbuffer{100};
    // maximum size of the particles buffer before it forces a flush
    particles_buffer_size_t flush_particles_maxbuffer{2000};
private:
    void create_events_datatype();
    void create_frames_datatype();
    void create_particles_datatype();

    void create_events_dataset(H5::H5File* file);
    void create_frames_dataset(H5::Group& group);
    void create_particles_dataset(H5::Group &group);

    void flush_events_buffer(bool forcedFlush=false);
    void flush_frames_buffer(bool forcedFlush=false);
    void flush_particles_buffer(bool forcedFlush=false);

    H5::H5File* m_file;
    H5::CompType m_eventsType;
    H5::CompType m_framesType;
    H5::CompType m_particlesType;
    H5::DataSet m_eventsDataSet;
    H5::DataSet m_framesDataSet;
    H5::DataSet m_particlesDataSet;

    std::string m_eventsTableName{"events"};
    std::string m_framesTableName{"frames"};
    std::string m_particlesTableName{"particles"};

    std::vector<hdf_event_header_t<FileType>> m_eventsBuffer;
    std::vector<hdf_frame_t<FileType>> m_framesBuffer;
    std::vector<hdf_particle_t<FileType>> m_particlesBuffer;

    // some used temporary objects
    H5::Group curEventGr;
};

template <urqmd_filetype FileType>
bool hdf5_exporter<FileType>::open(const std::string& filename)
{
    try {
        /*
         * Turn off the auto-printing when failure occurs so that we can
         * handle the errors appropriately
         */
        H5::Exception::dontPrint();

        close();

        // creates a new file if it doesnt exists
        m_file = new H5::H5File(filename, H5F_ACC_TRUNC);

        // create an attribute to hold the file type (possible values: 13,14,15 or 16)
        hsize_t dim[] = {1};   /* Dataspace dimensions */
        hsize_t      maxdims[1] = {1};
        H5::DataSpace space( 1, dim, maxdims);
        H5::DataType attrFileType(H5::PredType::NATIVE_SHORT);
        short fileTypeVal = short(FileType);

        H5::Attribute attrFile = m_file->createAttribute("filetype", attrFileType, space);
        attrFile.write(attrFileType, &fileTypeVal);

        /*
        * Create the events data space.
        */
        create_events_dataset(m_file);

        return true;
    }   // catch failure caused by the H5File operations
    catch( const H5::FileIException& error )
    {
        error.printError();
        return false;
    }
    // catch failure caused by the DataSet operations
    catch( const H5::DataSetIException& error )
    {
        error.printError();
        return false;
    }
    // catch failure caused by the DataSpace operations
    catch( const H5::DataSpaceIException& error )
    {
        error.printError();
        return false;
    }
    // catch failure caused by the DataType operations
    catch( const H5::DataTypeIException& error )
    {
        error.printError();
        return false;
    }
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::close()
{
    if(!m_file) return;

    // flush forced all remaining filled buffers
    flush_events_buffer(true);

    m_file->close();
    delete m_file;
    m_file = nullptr;
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::begin_event(uint event_id)
{
    static std::string eventGrpName{"event_"};

    std::string eventGroupPath = eventGrpName + std::to_string(event_id);

    curEventGr = m_file->createGroup(eventGroupPath, -1);

    create_frames_dataset(curEventGr);
    create_particles_dataset(curEventGr);
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::end_event()
{
    flush_frames_buffer(true);
    flush_particles_buffer(true);
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::begin_frame(uint frame_id)
{

}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::end_frame()
{

}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::create_events_datatype()
{
    /*
     * Create the memory datatype for events
     */
    m_eventsType = H5::CompType(sizeof(hdf_event_header_t<FileType>));
    m_eventsType.insertMember( "A1", HOFFSET(hdf_event_header_t<FileType>, A1), H5::PredType::NATIVE_UINT);
    m_eventsType.insertMember( "Z1", HOFFSET(hdf_event_header_t<FileType>, Z1), H5::PredType::NATIVE_UINT);
    m_eventsType.insertMember( "A2", HOFFSET(hdf_event_header_t<FileType>, A2), H5::PredType::NATIVE_UINT);
    m_eventsType.insertMember( "Z2", HOFFSET(hdf_event_header_t<FileType>, Z2), H5::PredType::NATIVE_UINT);
    m_eventsType.insertMember( "trans_beta_NN", HOFFSET(hdf_event_header_t<FileType>, transbNN), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "trans_beta_LAB", HOFFSET(hdf_event_header_t<FileType>, transbLab), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "trans_beta_PRO", HOFFSET(hdf_event_header_t<FileType>, transbPro), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "b_real", HOFFSET(hdf_event_header_t<FileType>, breal), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "b_min", HOFFSET(hdf_event_header_t<FileType>, bmin), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "b_max", HOFFSET(hdf_event_header_t<FileType>, bmax), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "xsection", HOFFSET(hdf_event_header_t<FileType>, xsection), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "EOS", HOFFSET(hdf_event_header_t<FileType>, eos), H5::PredType::NATIVE_UINT);
    m_eventsType.insertMember( "energy", HOFFSET(hdf_event_header_t<FileType>, Elab), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "s", HOFFSET(hdf_event_header_t<FileType>, s), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "momentum", HOFFSET(hdf_event_header_t<FileType>, Plab), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "seed", HOFFSET(hdf_event_header_t<FileType>, seed), H5::PredType::NATIVE_ULONG);
    m_eventsType.insertMember( "time", HOFFSET(hdf_event_header_t<FileType>, time), H5::PredType::NATIVE_UINT);
    m_eventsType.insertMember( "timestep", HOFFSET(hdf_event_header_t<FileType>, dtime), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "version", HOFFSET(hdf_event_header_t<FileType>, urqmd_version), H5::PredType::NATIVE_UINT);
    m_eventsType.insertMember( "version_min", HOFFSET(hdf_event_header_t<FileType>, urqmd_version_min), H5::PredType::NATIVE_UINT);
    m_eventsType.insertMember( "version_max", HOFFSET(hdf_event_header_t<FileType>, urqmd_version_max), H5::PredType::NATIVE_UINT);
    m_eventsType.insertMember( "CTParameters", HOFFSET(hdf_event_header_t<FileType>, CTPHandle), H5::VarLenType(&H5::PredType::NATIVE_DOUBLE));
    m_eventsType.insertMember( "CTOptions", HOFFSET(hdf_event_header_t<FileType>, CTOHandle), H5::VarLenType(&H5::PredType::NATIVE_UINT));
}

template<>
void hdf5_exporter<urqmd_filetype::F15>::create_events_datatype()
{
    m_eventsType = H5::CompType(sizeof(hdf_event_header_t<urqmd_filetype::F15>));
    m_eventsType.insertMember( "A1", HOFFSET(hdf_event_header_t<urqmd_filetype::F15>, A1), H5::PredType::NATIVE_UINT);
    m_eventsType.insertMember( "A2", HOFFSET(hdf_event_header_t<urqmd_filetype::F15>, A2), H5::PredType::NATIVE_UINT);
    m_eventsType.insertMember( "b", HOFFSET(hdf_event_header_t<urqmd_filetype::F15>, b), H5::PredType::NATIVE_FLOAT);
    m_eventsType.insertMember( "Snn", HOFFSET(hdf_event_header_t<urqmd_filetype::F15>, snn), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "xsection", HOFFSET(hdf_event_header_t<urqmd_filetype::F15>, xsection), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "energy", HOFFSET(hdf_event_header_t<urqmd_filetype::F15>, Elab), H5::PredType::NATIVE_DOUBLE);
    m_eventsType.insertMember( "momentum", HOFFSET(hdf_event_header_t<urqmd_filetype::F15>, Plab), H5::PredType::NATIVE_DOUBLE);
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::create_frames_datatype()
{
    /*
     * Create the memory datatype for frames
     */
    m_framesType = H5::CompType(sizeof(hdf_frame_t<FileType>));
    m_framesType.insertMember( "collisions", HOFFSET(hdf_frame_t<FileType>, Ncoll), H5::PredType::NATIVE_UINT);
    m_framesType.insertMember( "elastic_collisions", HOFFSET(hdf_frame_t<FileType>, Necoll), H5::PredType::NATIVE_UINT);
    m_framesType.insertMember( "inelastic_collisions", HOFFSET(hdf_frame_t<FileType>, Nicoll), H5::PredType::NATIVE_UINT);
    m_framesType.insertMember( "decays", HOFFSET(hdf_frame_t<FileType>, Ndecays), H5::PredType::NATIVE_UINT);
    m_framesType.insertMember( "pauli_collisions", HOFFSET(hdf_frame_t<FileType>, Npauli), H5::PredType::NATIVE_UINT);
    m_framesType.insertMember( "hard_resonances", HOFFSET(hdf_frame_t<FileType>, Nhard), H5::PredType::NATIVE_UINT);
    m_framesType.insertMember( "soft_resonances", HOFFSET(hdf_frame_t<FileType>, Nsoft), H5::PredType::NATIVE_UINT);
    m_framesType.insertMember( "other_resonances", HOFFSET(hdf_frame_t<FileType>, Nbar), H5::PredType::NATIVE_UINT);
    m_framesType.insertMember( "particles", HOFFSET(hdf_frame_t<FileType>, Npart), H5::PredType::NATIVE_UINT);
    m_framesType.insertMember( "time", HOFFSET(hdf_frame_t<FileType>, time), H5::PredType::NATIVE_DOUBLE);
}

template<>
void hdf5_exporter<urqmd_filetype::F15>::create_frames_datatype()
{
    /*
     * Create the memory datatype for frames
     */
    m_framesType = H5::CompType(sizeof(hdf_frame_t<urqmd_filetype::F15>));
    m_framesType.insertMember( "particles_in", HOFFSET(hdf_frame_t<urqmd_filetype::F15>, nIn), H5::PredType::NATIVE_UINT);
    m_framesType.insertMember( "particles_out", HOFFSET(hdf_frame_t<urqmd_filetype::F15>, nOut), H5::PredType::NATIVE_UINT);
    m_framesType.insertMember( "process_id", HOFFSET(hdf_frame_t<urqmd_filetype::F15>, pId), H5::PredType::NATIVE_UINT);
    m_framesType.insertMember( "collisions", HOFFSET(hdf_frame_t<urqmd_filetype::F15>, colls), H5::PredType::NATIVE_UINT);
    m_framesType.insertMember( "collision_time", HOFFSET(hdf_frame_t<urqmd_filetype::F15>, tcoll), H5::PredType::NATIVE_FLOAT);
    m_framesType.insertMember( "s", HOFFSET(hdf_frame_t<urqmd_filetype::F15>, s), H5::PredType::NATIVE_DOUBLE);
    m_framesType.insertMember( "xsection", HOFFSET(hdf_frame_t<urqmd_filetype::F15>, xsection), H5::PredType::NATIVE_DOUBLE);
    m_framesType.insertMember( "xsection_part", HOFFSET(hdf_frame_t<urqmd_filetype::F15>, xsection_part), H5::PredType::NATIVE_DOUBLE);
    m_framesType.insertMember( "density", HOFFSET(hdf_frame_t<urqmd_filetype::F15>, density), H5::PredType::NATIVE_DOUBLE);
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::create_particles_datatype()
{
    /*
     * Create the memory datatype for particle (f13)
     */
    m_particlesType = H5::CompType(sizeof(hdf_particle_t<FileType>));
    m_particlesType.insertMember( "t", HOFFSET(hdf_particle_t<FileType>, t), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "x", HOFFSET(hdf_particle_t<FileType>, x), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "y", HOFFSET(hdf_particle_t<FileType>, y), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "z", HOFFSET(hdf_particle_t<FileType>, z), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "E", HOFFSET(hdf_particle_t<FileType>, E), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "px", HOFFSET(hdf_particle_t<FileType>, px), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "py", HOFFSET(hdf_particle_t<FileType>, py), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "pz", HOFFSET(hdf_particle_t<FileType>, pz), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "mass", HOFFSET(hdf_particle_t<FileType>, mass), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "itype", HOFFSET(hdf_particle_t<FileType>, ityp), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "iso3", HOFFSET(hdf_particle_t<FileType>, dIz), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "charge", HOFFSET(hdf_particle_t<FileType>, chg), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "last_collision_partner", HOFFSET(hdf_particle_t<FileType>, lcl), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "collisions", HOFFSET(hdf_particle_t<FileType>, Ncol), H5::PredType::NATIVE_UINT);
    m_particlesType.insertMember( "parent_process_type", HOFFSET(hdf_particle_t<FileType>, ppr), H5::PredType::NATIVE_INT);

    m_particlesType.insertMember( "freezeout_t", HOFFSET(hdf_particle_t<FileType>, time_fr), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "freezeout_x", HOFFSET(hdf_particle_t<FileType>, x_fr), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "freezeout_y", HOFFSET(hdf_particle_t<FileType>, y_fr), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "freezeout_z", HOFFSET(hdf_particle_t<FileType>, z_fr), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "freezeout_E", HOFFSET(hdf_particle_t<FileType>, E_fr), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "freezeout_px", HOFFSET(hdf_particle_t<FileType>, px_fr), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "freezeout_py", HOFFSET(hdf_particle_t<FileType>, py_fr), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "freezeout_pz", HOFFSET(hdf_particle_t<FileType>, pz_fr), H5::PredType::NATIVE_DOUBLE);
}

template<>
void hdf5_exporter<urqmd_filetype::F14>::create_particles_datatype()
{
    /*
     * Create the memory datatype for particle (f14)
     */
    m_particlesType = H5::CompType(sizeof(hdf_particle_t<urqmd_filetype::F14>));
    m_particlesType.insertMember( "t", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, t), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "x", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, x), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "y", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, y), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "z", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, z), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "E", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, E), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "px", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, px), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "py", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, py), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "pz", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, pz), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "mass", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, mass), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "itype", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, ityp), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "iso3", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, dIz), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "charge", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, chg), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "last_collision_partner", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, lcl), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "collisions", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, Ncol), H5::PredType::NATIVE_UINT);
    m_particlesType.insertMember( "parent_process_type", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, ppr), H5::PredType::NATIVE_INT);

    m_particlesType.insertMember( "decay_time", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, tdecay), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "formation_time", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, tform), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "xsection_reduction", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, xsection_red), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "unique_nr", HOFFSET(hdf_particle_t<urqmd_filetype::F14>, unique_id), H5::PredType::NATIVE_INT);
}

template<>
void hdf5_exporter<urqmd_filetype::F15>::create_particles_datatype()
{
    /*
     * Create the memory datatype for particle (F15)
     */
    m_particlesType = H5::CompType(sizeof(hdf_particle_t<urqmd_filetype::F15>));
    m_particlesType.insertMember( "frame", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, frame_id), H5::PredType::NATIVE_UINT);
    m_particlesType.insertMember( "index", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, id), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "t", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, t), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "x", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, x), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "y", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, y), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "z", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, z), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "E", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, E), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "px", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, px), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "py", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, py), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "pz", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, pz), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "mass", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, mass), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "itype", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, ityp), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "iso3", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, dIz), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "charge", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, chg), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "last_collision_partner", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, lcl), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "collisions", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, Ncol), H5::PredType::NATIVE_UINT);

    m_particlesType.insertMember( "strangeness", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, S), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "history", HOFFSET(hdf_particle_t<urqmd_filetype::F15>, history), H5::PredType::NATIVE_LONG);
}

template<>
void hdf5_exporter<urqmd_filetype::F16>::create_particles_datatype()
{
    /*
     * Create the memory datatype for particle (F16)
     */
    m_particlesType = H5::CompType(sizeof(hdf_particle_t<urqmd_filetype::F16>));
    m_particlesType.insertMember( "t", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, t), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "x", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, x), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "y", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, y), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "z", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, z), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "E", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, E), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "px", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, px), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "py", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, py), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "pz", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, pz), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "mass", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, mass), H5::PredType::NATIVE_DOUBLE);
    m_particlesType.insertMember( "itype", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, ityp), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "iso3", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, dIz), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "charge", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, chg), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "last_collision_partner", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, lcl), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "collisions", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, Ncol), H5::PredType::NATIVE_UINT);
    m_particlesType.insertMember( "parent_process_type", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, ppr), H5::PredType::NATIVE_INT);

    m_particlesType.insertMember( "parent_id1", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, ityp1), H5::PredType::NATIVE_INT);
    m_particlesType.insertMember( "parent_id2", HOFFSET(hdf_particle_t<urqmd_filetype::F16>, ityp2), H5::PredType::NATIVE_INT);
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::create_events_dataset(H5::H5File* file)
{
    /*
    * Create the data space.
    */
    hsize_t dim[] = {0};   /* Dataspace dimensions */
    hsize_t      maxdims[1] = {H5S_UNLIMITED};
    H5::DataSpace space( 1, dim, maxdims);

    H5::DSetCreatPropList cparms;
    hsize_t chunk_dims[1] = { 1 };
    cparms.setChunk( 1, chunk_dims);

    m_eventsDataSet = file->createDataSet(m_eventsTableName, m_eventsType, space, cparms);
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::create_frames_dataset(H5::Group &group)
{
    /*
    * Create the data space.
    */
    hsize_t dim[] = {0};   /* Dataspace dimensions */
    hsize_t      maxdims[1] = {H5S_UNLIMITED};
    H5::DataSpace space( 1, dim, maxdims);

    H5::DSetCreatPropList cparms;
    hsize_t chunk_dims[1] = { 1 };
    cparms.setChunk( 1, chunk_dims);

    m_framesDataSet = group.createDataSet(m_framesTableName, m_framesType, space, cparms);
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::create_particles_dataset(H5::Group& group)
{
    /*
    * Create the data space.
    */
    hsize_t dim[] = {0};   /* Dataspace dimensions */
    hsize_t      maxdims[1] = {H5S_UNLIMITED};
    H5::DataSpace space( 1, dim, maxdims);

    H5::DSetCreatPropList cparms;
    hsize_t chunk_dims[1] = { 1 };
    cparms.setChunk( 1, chunk_dims);

    m_particlesDataSet = group.createDataSet(m_particlesTableName, m_particlesType, space, cparms);
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::flush_events_buffer(bool forcedFlush)
{
    if((forcedFlush && m_eventsBuffer.size()>0) || m_eventsBuffer.size()>=flush_events_maxbuffer){

        /* Extend the dataset.*/
        hsize_t curSize[1], newSize[1];
        m_eventsDataSet.getSpace().getSimpleExtentDims(curSize);

        newSize[0] = curSize[0] + m_eventsBuffer.size();
        m_eventsDataSet.extend( newSize );

        /* Select a hyperslab (Slice).*/
        hsize_t     offset[1], dims[1];
        offset[0] = curSize[0];
        dims[0] = m_eventsBuffer.size();            /* data dimensions */

        H5::DataSpace slice = m_eventsDataSet.getSpace();
        slice.selectHyperslab( H5S_SELECT_SET, dims, offset );

        /* Define memory space */
        H5::DataSpace space( 1, dims );

        m_eventsDataSet.write(m_eventsBuffer.data(), m_eventsType, space, slice);
        m_eventsBuffer.clear();
    }
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::flush_frames_buffer(bool forcedFlush)
{
    if((forcedFlush && m_framesBuffer.size()>0) || m_framesBuffer.size()>=flush_frames_maxbuffer){

        /* Extend the dataset.*/
        hsize_t curSize[1], newSize[1];
        m_framesDataSet.getSpace().getSimpleExtentDims(curSize);

        newSize[0] = curSize[0] + m_framesBuffer.size();
        m_framesDataSet.extend( newSize );

        /* Select a hyperslab (Slice).*/
        hsize_t     offset[1], dims[1];
        offset[0] = curSize[0];
        dims[0] = m_framesBuffer.size();            /* data dimensions */

        H5::DataSpace slice = m_framesDataSet.getSpace();
        slice.selectHyperslab( H5S_SELECT_SET, dims, offset );

        /* Define memory space */
        H5::DataSpace space( 1, dims );

        m_framesDataSet.write(m_framesBuffer.data(), m_framesType, space, slice);
        m_framesBuffer.clear();
    }
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::flush_particles_buffer(bool forcedFlush)
{
    if((forcedFlush && m_particlesBuffer.size()>0) || m_particlesBuffer.size()>=flush_particles_maxbuffer){

        /* Extend the dataset.*/
        hsize_t curSize[1], newSize[1];
        m_particlesDataSet.getSpace().getSimpleExtentDims(curSize);

        newSize[0] = curSize[0] + m_particlesBuffer.size();
        m_particlesDataSet.extend( newSize );

        /* Select a hyperslab (Slice).*/
        hsize_t     offset[1], dims[1];
        offset[0] = curSize[0];
        dims[0] = m_particlesBuffer.size();            /* data dimensions */

        H5::DataSpace slice = m_particlesDataSet.getSpace();
        slice.selectHyperslab( H5S_SELECT_SET, dims, offset );

        /* Define memory space */
        H5::DataSpace space( 1, dims );

        m_particlesDataSet.write(m_particlesBuffer.data(), m_particlesType, space, slice);
        m_particlesBuffer.clear();
    }
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::write_header(const urqmd_event_header<FileType> &header)
{
    hdf_event_header_t<FileType> buf;
    copy(header, buf);

    m_eventsBuffer.push_back(buf);

    flush_events_buffer();
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::write_frame(uint event_id, const urqmd_frame<FileType> &frame)
{
    hdf_frame_t<FileType> buf;
    copy(frame, buf);

    m_framesBuffer.push_back(buf);

    m_particlesBuffer.reserve(buf.Npart);

    flush_frames_buffer();
}

template<>
void hdf5_exporter<urqmd_filetype::F15>::write_frame(uint event_id, const urqmd_frame<urqmd_filetype::F15> &frame)
{
    hdf_frame_t<urqmd_filetype::F15> buf;
    copy(frame, buf);

    m_framesBuffer.push_back(buf);

    flush_frames_buffer();
}

template<>
void hdf5_exporter<urqmd_filetype::F16>::write_frame(uint event_id, const urqmd_frame<urqmd_filetype::F16> &frame)
{
    hdf_frame_t<urqmd_filetype::F16> buf;
    copy(frame, buf);

    m_framesBuffer.push_back(buf);

    flush_frames_buffer();
}

template<urqmd_filetype FileType>
void hdf5_exporter<FileType>::write_particle(uint event_id, uint frame_id, const urqmd_particle<FileType> &particle)
{
    hdf_particle_t<FileType> buf;
    copy(particle, buf);

    m_particlesBuffer.push_back(buf);

    flush_particles_buffer();
}

template<>
void hdf5_exporter<urqmd_filetype::F15>::write_particle(uint event_id, uint frame_id, const urqmd_particle<urqmd_filetype::F15> &particle)
{
    hdf_particle_t<urqmd_filetype::F15> buf;
    buf.frame_id = frame_id;
    copy(particle, buf);

    m_particlesBuffer.push_back(buf);

    flush_particles_buffer();
}


#endif // HDF5_EXPORTER_H
