/**
 ** This file is part of the urqmdfc project.
 ** Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef HDF5_EXPORT_TYPES_H
#define HDF5_EXPORT_TYPES_H

#include "urqmd_types.h"
#include "urqmd_standard_header.h"
#include "urqmd_frame.h"
#include "urqmd_particle.h"

#include "H5Cpp.h"


//==================================== WARNING =====================================================
// ******* DO NOT ******* use the structs below, prefer the C++ templated versions: *******
// - urqmd_event_header<FileType>, urqmd_frame<FileType>, urqmd_particle<FileType>
//
// The ones below are used for HDF5 exporter which needs simple data format writting
//records.
//
//==================================================================================================

//==================================== HDF Record(s)
// templated version starts here!
// standard event header
template <urqmd_filetype FileType>
struct hdf_event_header_t
{
    uint A1;// projectile
    uint Z1;
    uint A2;// target
    uint Z2;
    double transbNN; // transformation betas
    double transbLab;
    double transbPro;
    double breal;// real impact parameter
    double bmin;// min impact parameter
    double bmax;// max impact parameter
    double xsection; //cross section
    uint eos; // equation of state
    double Elab; // energy lab
    double s;  // sqrt(s) in lab
    double Plab; // momentum lab
    ulong seed; // random seed
    uint time; // total time
    double dtime; // time step
    uint urqmd_version_min;
    uint urqmd_version_max;
    uint urqmd_version;
    hvl_t CTPHandle; // CTParameters
    hvl_t CTOHandle; // CTOptions
};


// the header for f15
template <>
struct hdf_event_header_t<urqmd_filetype::F15>
{
    uint A1;
    uint A2;
    float b;
    double snn;
    double xsection;
    double Elab;
    double Plab;
};

//==================================== Frame Record(s)
// standard frame info
template <urqmd_filetype FileType>
struct hdf_frame_t {
    uint Ncoll; // # of collisions
    uint Necoll; // # of  elastic collisions
    uint Nicoll; // # of inelastic collisions
    uint Ndecays; // # of decays
    uint Npauli; // # of Pauli-blocked collisions
    uint Nhard; // # of produced hard baryon resonances
    uint Nsoft; // # of produced soft baryon resonances
    uint Nbar; // # of baryon resonances produced via a decay of another resonance
    uint Npart;
    double time;
};

// f15 frame info
template <>
struct hdf_frame_t<urqmd_filetype::F15>
{
    uint nIn;
    uint nOut;
    uint pId;
    uint colls;
    float tcoll;
    double s;
    double xsection;
    double xsection_part;
    double density;
};

//==================================== Particle Record(s)
template <urqmd_filetype FileType>
struct hdf_particle_t
{
    double t;
    double x;
    double y;
    double z;
    double E;
    double px;
    double py;
    double pz;
    double mass;
    int ityp; // particle type
    int dIz; // (doubled) Isospin z-projection
    int chg; // charge
    int lcl; // idx last collision partner
    uint Ncol; // number of collisions
    int ppr; // parent process type
    double time_fr; // freeze-out time of particle
    double x_fr; // freeze-out x position of particle
    double y_fr; // freeze-out y position of particle
    double z_fr; // freeze-out z position of particle
    double E_fr; // freeze-out energy of particle
    double px_fr; // freeze-out px of particle
    double py_fr; // freeze-out py of particle
    double pz_fr; // freeze-out pz of particle
};

template <>
struct hdf_particle_t<urqmd_filetype::F14>
{
    double t;
    double x;
    double y;
    double z;
    double E;
    double px;
    double py;
    double pz;
    double mass;
    int ityp; // particle type
    int dIz; // (doubled) Isospin z-projection
    int chg; // charge
    int lcl; // idx last collision partner
    uint Ncol; // number of collisions
    int ppr; // parent process type
    double tdecay;
    double tform;
    double xsection_red;
    int unique_id;
};

template <>
struct hdf_particle_t<urqmd_filetype::F15>
{
    uint frame_id;
    int id;
    double t;
    double x;
    double y;
    double z;
    double E;
    double px;
    double py;
    double pz;
    double mass;
    int ityp; // particle type
    int dIz; // (doubled) Isospin z-projection
    int chg; // charge
    int lcl; // idx last collision partner
    uint Ncol; // number of collisions
    int S; // strangeness
    long int history; // history information (for debugging)
};

template <>
struct hdf_particle_t<urqmd_filetype::F16>
{
    double t;
    double x;
    double y;
    double z;
    double E;
    double px;
    double py;
    double pz;
    double mass;
    int ityp; // particle type
    int dIz; // (doubled) Isospin z-projection
    int chg; // charge
    int lcl; // idx last collision partner
    uint Ncol; // number of collisions
    int ppr; // parent process type
    int ityp1; // ityp-old 1 : particle-ID of parent particle # 1
    int ityp2; // ityp-old 1 : particle-ID of parent particle # 1
};

//==================================== Copy functions from templated version to HDF Record(s)

template <urqmd_filetype FileType>
inline void copy(const urqmd_event_header<FileType>& fromHeader, hdf_event_header_t<FileType>& toHeader)
{
    toHeader.A1 = fromHeader.projectile_A();
    toHeader.Z1 = fromHeader.projectile_Z();
    toHeader.A2 = fromHeader.target_A();
    toHeader.Z2 = fromHeader.target_Z();
    toHeader.transbNN = fromHeader.trans_beta_NN();
    toHeader.transbLab = fromHeader.trans_beta_Lab();
    toHeader.transbPro = fromHeader.trans_beta_Pro();
    toHeader.breal = fromHeader.b();
    toHeader.bmin = fromHeader.b_min();
    toHeader.bmax = fromHeader.b_max();
    toHeader.xsection = fromHeader.xsection();
    toHeader.eos = fromHeader.EOS();
    toHeader.Elab = fromHeader.energy();
    toHeader.s = fromHeader.ECM();
    toHeader.Plab = fromHeader.momentum();
    toHeader.seed = fromHeader.seed();
    toHeader.time = fromHeader.time();
    toHeader.dtime= fromHeader.timestep();
    toHeader.urqmd_version = fromHeader.version();
    toHeader.urqmd_version_min = fromHeader.version_min();
    toHeader.urqmd_version_max = fromHeader.version_max();

    toHeader.CTPHandle.p = const_cast<double*>(fromHeader.parameters().data());
    toHeader.CTPHandle.len = fromHeader.parameters_count();

    toHeader.CTOHandle.p = const_cast<uint*>(fromHeader.options().data());
    toHeader.CTOHandle.len = fromHeader.options_count();
}

//template <>
//void copy(const urqmd_event_header<urqmd_filetype::F15>& fromHeader, hdf_event_header_t& toHeader)
//{
//       static_assert(true, "Can not copy a F15 to hdf_event_header_t. You must copy to hdf_event_header_f15_t");
//}

template <>
inline void copy(const urqmd_event_header<urqmd_filetype::F15>& fromHeader, hdf_event_header_t<urqmd_filetype::F15>& toHeader)
{
    toHeader.A1 = fromHeader.projectile_A();
    toHeader.A2 = fromHeader.target_A();
    toHeader.b  = fromHeader.b();
    toHeader.snn= fromHeader.ECM();
    toHeader.xsection = fromHeader.xsection();
    toHeader.Elab = fromHeader.energy();
    toHeader.Plab = fromHeader.momentum();
}

template <urqmd_filetype FileType>
inline void copy(const urqmd_frame<FileType>& fromFrame, hdf_frame_t<FileType>& toFrame)
{
    //    static_assert(FileType==urqmd_filetype::F15, "Can not copy a F15 to hdf_frame_t. You must copy to hdf_frame_f15_t");

    toFrame.Ncoll   = fromFrame.collisions();
    toFrame.Necoll  = fromFrame.collisions_elastic();
    toFrame.Nicoll  = fromFrame.collisions_inelastic();
    toFrame.Ndecays = fromFrame.decays();
    toFrame.Npauli  = fromFrame.collisions_pauli();
    toFrame.Nhard   = fromFrame.resonances_hard();
    toFrame.Nsoft   = fromFrame.resonances_soft();
    toFrame.Nbar    = fromFrame.resonances_other();
    toFrame.Npart   = fromFrame.particles();
    toFrame.time    = fromFrame.time();
}

template <>
inline void copy(const urqmd_frame<urqmd_filetype::F15>& fromFrame, hdf_frame_t<urqmd_filetype::F15>& toFrame)
{
    toFrame.nIn = fromFrame.particles_in();
    toFrame.nOut= fromFrame.particles_out();
    toFrame.pId = fromFrame.processId();
    toFrame.colls = fromFrame.collisions();
    toFrame.tcoll = fromFrame.collision_time();
    toFrame.s = fromFrame.ECM();
    toFrame.xsection = fromFrame.xsection();
    toFrame.xsection_part = fromFrame.xsection_partial();
    toFrame.density = fromFrame.density();
}

template <urqmd_filetype FileType>
inline void copy(const urqmd_particle<FileType>& fromParticle, hdf_particle_t<FileType>& toParticle)
{
    toParticle.t = fromParticle.t();
    toParticle.x = fromParticle.x();
    toParticle.y = fromParticle.y();
    toParticle.z = fromParticle.z();
    toParticle.E = fromParticle.energy();
    toParticle.px = fromParticle.px();
    toParticle.py = fromParticle.py();
    toParticle.pz = fromParticle.pz();
    toParticle.mass = fromParticle.mass();
    toParticle.ityp = fromParticle.ityp();
    toParticle.dIz = fromParticle.iso3();
    toParticle.chg = fromParticle.charge();
    toParticle.lcl = fromParticle.collision_parent();
    toParticle.Ncol = fromParticle.collisions();

    toParticle.ppr = fromParticle.parent_proc_id();
    toParticle.time_fr = fromParticle.freezeout_time();
    toParticle.x_fr = fromParticle.freezeout_x();
    toParticle.y_fr = fromParticle.freezeout_y();
    toParticle.z_fr = fromParticle.freezeout_z();
    toParticle.E_fr = fromParticle.freezeout_energy();
    toParticle.px_fr = fromParticle.freezeout_px();
    toParticle.py_fr = fromParticle.freezeout_py();
    toParticle.pz_fr = fromParticle.freezeout_pz();
}

template <>
inline void copy(const urqmd_particle<urqmd_filetype::F14>& fromParticle, hdf_particle_t<urqmd_filetype::F14>& toParticle)
{
    toParticle.t = fromParticle.t();
    toParticle.x = fromParticle.x();
    toParticle.y = fromParticle.y();
    toParticle.z = fromParticle.z();
    toParticle.E = fromParticle.energy();
    toParticle.px = fromParticle.px();
    toParticle.py = fromParticle.py();
    toParticle.pz = fromParticle.pz();
    toParticle.mass = fromParticle.mass();
    toParticle.ityp = fromParticle.ityp();
    toParticle.dIz = fromParticle.iso3();
    toParticle.chg = fromParticle.charge();
    toParticle.lcl = fromParticle.collision_parent();
    toParticle.Ncol = fromParticle.collisions();

    toParticle.ppr = fromParticle.parent_proc_id();

    auto extendedInfo = fromParticle.extendedInfo();

    if(extendedInfo){
        toParticle.tdecay = extendedInfo->t_decay();
        toParticle.tform = extendedInfo->t_formation();
        toParticle.xsection_red = extendedInfo->xsection_reduction();
        toParticle.unique_id = extendedInfo->unique_i();
    }
    else {
        toParticle.tdecay = 0;
        toParticle.tform = 0;
        toParticle.xsection_red = 0;
        toParticle.unique_id = 0;
    }
}

template <>
inline void copy(const urqmd_particle<urqmd_filetype::F15>& fromParticle, hdf_particle_t<urqmd_filetype::F15>& toParticle)
{
    toParticle.t = fromParticle.t();
    toParticle.x = fromParticle.x();
    toParticle.y = fromParticle.y();
    toParticle.z = fromParticle.z();
    toParticle.E = fromParticle.energy();
    toParticle.px = fromParticle.px();
    toParticle.py = fromParticle.py();
    toParticle.pz = fromParticle.pz();
    toParticle.mass = fromParticle.mass();
    toParticle.ityp = fromParticle.ityp();
    toParticle.dIz = fromParticle.iso3();
    toParticle.chg = fromParticle.charge();
    toParticle.lcl = fromParticle.collision_parent();
    toParticle.Ncol = fromParticle.collisions();

    toParticle.id = fromParticle.index();
    toParticle.S = fromParticle.strangeness();
    toParticle.history = fromParticle.history();
}

template <>
inline void copy(const urqmd_particle<urqmd_filetype::F16>& fromParticle, hdf_particle_t<urqmd_filetype::F16>& toParticle)
{
    toParticle.t = fromParticle.t();
    toParticle.x = fromParticle.x();
    toParticle.y = fromParticle.y();
    toParticle.z = fromParticle.z();
    toParticle.E = fromParticle.energy();
    toParticle.px = fromParticle.px();
    toParticle.py = fromParticle.py();
    toParticle.pz = fromParticle.pz();
    toParticle.mass = fromParticle.mass();
    toParticle.ityp = fromParticle.ityp();
    toParticle.dIz = fromParticle.iso3();
    toParticle.chg = fromParticle.charge();
    toParticle.lcl = fromParticle.collision_parent();
    toParticle.Ncol = fromParticle.collisions();

    toParticle.ppr = fromParticle.parent_proc_id();

    const auto& extendedInfo = fromParticle.extendedInfo();

    if(extendedInfo){
        toParticle.ityp1 = extendedInfo->parent_ityp1();
        toParticle.ityp2 = extendedInfo->parent_ityp2();
    }
    else {
        toParticle.ityp1 = 0;
        toParticle.ityp2 = 0;
    }
}

#endif // HDF5_EXPORT_TYPES_H
