/**
 ** This file is part of the urqmdfc project.
 ** Copyright 2018 Mihai Niculescu <mihai@spacescience.ro>.
 **
 ** This program is free software: you can redistribute it and/or modify
 ** it under the terms of the GNU General Public License as published by
 ** the Free Software Foundation, either version 3 of the License, or
 ** (at your option) any later version.
 **
 ** This program is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 ** GNU General Public License for more details.
 **
 ** You should have received a copy of the GNU General Public License
 ** along with this program.  If not, see <http://www.gnu.org/licenses/>.
 **/

#ifndef URQMD_EXPORTER_H
#define URQMD_EXPORTER_H

#include <fstream>

#include "urqmd_types.h"
#include "utils.h"
#include "urqmd_file_exporter.h"

#include "urqmd_standard_header.h"
#include "urqmd_frame.h"
#include "urqmd_particle.h"

template <urqmd_filetype FileType>
class urqmd_exporter : public urqmd_base_exporter<FileType>
{
public:
    urqmd_exporter(){}

    urqmd_base_exporter<FileType>* clone() override { return new urqmd_exporter<FileType>; }
    std::string file_extension() const override;

    bool open(const std::string &filename) override
    {
        os.open(filename);

        return static_cast<bool>(os);
    }

    void close() override
    {
        os.close();
    }

    void begin_event(uint event_id) override { }
    void end_event() override { }

    void begin_frame(uint event_id) override { }
    void end_frame() override { }

    void write_header(const urqmd_event_header<FileType>& header) override;
    void write_frame(uint event_id, const urqmd_frame<FileType>& frame) override;
    void write_particle(uint event_id, uint frame_id, const urqmd_particle<FileType>& particle) override;

private:
    urqmd_frame<FileType> curframe; // temporary frame used when writting F16
    std::string filename;
    std::ofstream os;
};


template<urqmd_filetype FileType>
std::string urqmd_exporter<FileType>::file_extension() const
{
    return "urqmd";
}

template<>
std::string urqmd_exporter<urqmd_filetype::F13>::file_extension() const
{
    return "f13";
}

template<>
std::string urqmd_exporter<urqmd_filetype::F14>::file_extension() const
{
    return "f14";
}

template<>
std::string urqmd_exporter<urqmd_filetype::F15>::file_extension() const
{
    return "f15";
}

template<>
std::string urqmd_exporter<urqmd_filetype::F16>::file_extension() const
{
    return "f16";
}

template <urqmd_filetype FileType>
void urqmd_exporter<FileType>::write_header(const urqmd_event_header<FileType>& header)
{
    os << "UQMD   version:     " <<  format<uint>( 7, header.version()) <<  format<uint>( 7, header.version_min()) <<  format<uint>( 7, header.version_max())
       << "  output_file  " <<  format<uint>( 2, header.type())
       << '\n';

    os << "projectile:  (mass, char) " <<  format<uint>( 4, header.projectile_A()) <<  format<uint>( 4, header.projectile_Z())
       << "   target:  (mass, char) "  <<  format<uint>( 4, header.target_A()) <<  format<uint>( 4, header.target_Z())
       << " \n";

    os << "transformation betas (NN,lab,pro)   " <<  format<double>( 11, 7, header.trans_beta_NN() ) <<  format<double>( 11, 7, header.trans_beta_Lab() ) <<  format<double>( 11, 7, header.trans_beta_Pro() )
       << '\n';

    os << "impact_parameter_real/min/max(fm):  " <<  format<double>( 6, 2, header.b()) <<  format<double>( 6, 2, header.b_min()) <<  format<double>( 6, 2, header.b_max())
       << "  total_cross_section(mbarn):  " <<  format<double>( 9, 2, header.xsection())
       << '\n';

    os << "equation_of_state:  " <<  format<uint>( 3, header.EOS())
       << "  E_lab(GeV/u):" << sci_format<double>( 11, 4, header.energy())
       << "  sqrt(s)(GeV):" << sci_format<double>( 11, 4, header.ECM())
       << "  p_lab(GeV/u):" << sci_format<double>( 11, 4, header.momentum())
       << '\n';

    os << "event# " <<  format<uint>( 9, header.id())
       << " random seed:" <<  format<uint>( 12, header.seed())
       << " (auto)  "
       << " total_time(fm/c):  " <<  format<uint>( 7, header.time())
       << " Delta(t)_O(fm/c):  " <<  format<double>( 11, 3, header.timestep())
       << '\n';

    uint nlines = header.options_count()/15;
    for(uint i=0; i<nlines; ++i){
        os << "op";
        for(int j=0; j<15; ++j) os <<  format<uint>(3, header.option(i*15+j)) << "  ";
        os << '\n';
    }

    uint mlines = header.parameters_count()/12;
    for(uint i=0; i<mlines; ++i){
        os << "pa";
        for(int j=0; j<12; ++j) os << sci_format<double>( 11, 4, header.parameter(i*12+j)) << "  ";
        os << '\n';
    }

    os << "pvec: r0              rx              ry              rz              p0              px              py              pz              m          ityp 2i3 chg lcl#  ncl or \n";
}

template <>
void urqmd_exporter<urqmd_filetype::F15>::write_header(const urqmd_event_header<urqmd_filetype::F15>& header)
{
    os << "      -1" <<  format<uint>( 8, header.id())
       <<  format<uint>( 4, header.projectile_A()) <<  format<uint>( 7, header.target_A())
       <<  format<double>( 8, 3, header.b())
       << sci_format<double>( 12, 4, header.ECM())
       << sci_format<double>( 12, 4, header.xsection())
       << sci_format<double>( 12, 4, header.energy())
       << sci_format<double>( 12, 4, header.momentum())
       << '\n';

}

template <urqmd_filetype FileType>
void urqmd_exporter<FileType>::write_frame(uint /*evId*/, const urqmd_frame<FileType>& frame)
{
    os <<  format<uint>(12, frame.particles()) <<  format<uint>(12, frame.time()) << '\n';
    os <<  format<uint>(8, frame.collisions())
       <<  format<uint>(8, frame.collisions_elastic())
       <<  format<uint>(8, frame.collisions_inelastic())
       <<  format<uint>(8, frame.decays())
       <<  format<uint>(8, frame.collisions_pauli())
       <<  format<uint>(8, frame.resonances_hard())
       <<  format<uint>(8, frame.resonances_soft())
       <<  format<uint>(8, frame.resonances_other())
       << '\n';
}

template <>
void urqmd_exporter<urqmd_filetype::F15>::write_frame(uint /*evId*/,  const urqmd_frame<urqmd_filetype::F15>& frame)
{
    os <<  format<uint>(8, frame.particles_in())
       <<  format<uint>(8, frame.particles_out())
       <<  format<uint>(4, frame.processId())
       <<  format<uint>(7, frame.collisions())
       <<  format<float>(8, 3, frame.collision_time())
       << sci_format<double>(12, 4, frame.ECM())
       << sci_format<double>(12, 4, frame.xsection())
       << sci_format<double>(12, 4, frame.xsection_partial())
       << sci_format<double>(12, 4, frame.density())
       << '\n';
}

template <>
void urqmd_exporter<urqmd_filetype::F16>::write_frame(uint /*evId*/,  const urqmd_frame<urqmd_filetype::F16>& frame)
{
    // we just copy it in the memory
    curframe = frame; // we'll write the frame when end_frame() is called
}

template <>
void urqmd_exporter<urqmd_filetype::F16>::end_frame()
{
    os << 'E'<<  format<uint>(8, curframe.collisions())
       <<  format<uint>(8, curframe.collisions_elastic())
       <<  format<uint>(8, curframe.collisions_inelastic())
       <<  format<uint>(8, curframe.decays())
       <<  format<uint>(8, curframe.collisions_pauli())
       <<  format<uint>(8, curframe.resonances_hard())
       <<  format<uint>(8, curframe.resonances_soft())
       <<  format<uint>(8, curframe.resonances_other())
       << '\n';
}

template <urqmd_filetype FileType>
void urqmd_exporter<FileType>::write_particle(uint /*evId*/,  uint /*frameId*/, const urqmd_particle<FileType>& particle)
{ }

template <>
void urqmd_exporter<urqmd_filetype::F13>::write_particle(uint /*evId*/,  uint /*frameId*/, const urqmd_particle<urqmd_filetype::F13>& particle)
{
    os << sci_format<double>(16,8, particle.t())
       << sci_format<double>(16,8, particle.x())
       << sci_format<double>(16,8, particle.y())
       << sci_format<double>(16,8, particle.z())
       << sci_format<double>(16,8, particle.energy())
       << sci_format<double>(16,8, particle.px())
       << sci_format<double>(16,8, particle.py())
       << sci_format<double>(16,8, particle.pz())
       << sci_format<double>(16,8, particle.mass())
       <<  format<int>(11,particle.ityp())
       <<  format<int>(3,particle.iso3())
       <<  format<int>(3,particle.charge())
       <<  format<int>(9,particle.collision_parent())
       <<  format<uint>(5,particle.collisions())

       <<  format<int>(4,particle.parent_proc_id())
       << sci_format<double>(16, 8, particle.freezeout_time())
       << sci_format<double>(16, 8, particle.freezeout_x())
       << sci_format<double>(16, 8, particle.freezeout_y())
       << sci_format<double>(16, 8, particle.freezeout_z())
       << sci_format<double>(16, 8, particle.freezeout_energy())
       << sci_format<double>(16, 8, particle.freezeout_px())
       << sci_format<double>(16, 8, particle.freezeout_py())
       << sci_format<double>(16, 8, particle.freezeout_pz())
       << '\n';
}

template <>
void urqmd_exporter<urqmd_filetype::F14>::write_particle(uint /*evId*/,  uint /*frameId*/, const urqmd_particle<urqmd_filetype::F14>& particle)
{
    os << sci_format<double>(16,8, particle.t())
       << sci_format<double>(16,8, particle.x())
       << sci_format<double>(16,8, particle.y())
       << sci_format<double>(16,8, particle.z())
       << sci_format<double>(16,8, particle.energy())
       << sci_format<double>(16,8, particle.px())
       << sci_format<double>(16,8, particle.py())
       << sci_format<double>(16,8, particle.pz())
       << sci_format<double>(16,8, particle.mass())
       <<  format<int>(11,particle.ityp())
       <<  format<int>(3,particle.iso3())
       <<  format<int>(3,particle.charge())
       <<  format<int>(9,particle.collision_parent())
       <<  format<uint>(5,particle.collisions());

    auto extendedInfo = particle.extendedInfo();
    if(static_cast<bool>(extendedInfo)){
        os <<  format<int>(10, particle.parent_proc_id())
           << sci_format<double>(16, 8, extendedInfo->t_decay())
           << sci_format<double>(16, 8, extendedInfo->t_formation())
           << sci_format<double>(16, 8, extendedInfo->xsection_reduction())
           <<  format<int>(8, extendedInfo->unique_i())
           << '\n';
    }
    else {
        os <<  format<int>(4, particle.parent_proc_id())
           << '\n';
    }

}

template <>
void urqmd_exporter<urqmd_filetype::F15>::write_particle(uint /*evId*/,  uint /*frameId*/, const urqmd_particle<urqmd_filetype::F15>& particle)
{
    os <<  format<int>(5, particle.index())
       << sci_format<double>(16,8, particle.t())
       << sci_format<double>(16,8, particle.x())
       << sci_format<double>(16,8, particle.y())
       << sci_format<double>(16,8, particle.z())
       << sci_format<double>(16,8, particle.energy())
       << sci_format<double>(16,8, particle.px())
       << sci_format<double>(16,8, particle.py())
       << sci_format<double>(16,8, particle.pz())
       << sci_format<double>(16,8, particle.mass())
       <<  format<int>(11,particle.ityp())
       <<  format<int>(3,particle.iso3())
       <<  format<int>(3,particle.charge())
       <<  format<int>(9,particle.collision_parent())
       <<  format<uint>(5,particle.collisions())

       <<  format<int>(3,particle.strangeness())
       <<  format<int>(15,particle.history())
       << '\n';

}

template <>
void urqmd_exporter<urqmd_filetype::F16>::write_particle(uint /*evId*/,  uint /*frameId*/, const urqmd_particle<urqmd_filetype::F16>& particle)
{
    auto extendedInfo = particle.extendedInfo();
    if(extendedInfo){
        os << sci_format<double>(15,7, particle.t())
           << sci_format<double>(15,7, particle.x())
           << sci_format<double>(15,7, particle.y())
           << sci_format<double>(15,7, particle.z())
           << sci_format<double>(15,7, particle.energy())
           << sci_format<double>(15,7, particle.px())
           << sci_format<double>(15,7, particle.py())
           << sci_format<double>(15,7, particle.pz())
           << sci_format<double>(15,7, particle.mass())
           <<  format<int>(11,particle.ityp())
           <<  format<int>(3,particle.iso3())
           <<  format<int>(3,particle.charge())
           <<  format<int>(9,particle.collision_parent())
           <<  format<uint>(5,particle.collisions())
           <<  format<int>(4,particle.parent_proc_id())
           <<  format<int>(4,extendedInfo->parent_ityp1())
           <<  format<int>(4,extendedInfo->parent_ityp2())
           << '\n';

    }
    else {
        os << sci_format<double>(16,8, particle.t())
           << sci_format<double>(16,8, particle.x())
           << sci_format<double>(16,8, particle.y())
           << sci_format<double>(16,8, particle.z())
           << sci_format<double>(16,8, particle.energy())
           << sci_format<double>(16,8, particle.px())
           << sci_format<double>(16,8, particle.py())
           << sci_format<double>(16,8, particle.pz())
           << sci_format<double>(16,8, particle.mass())
           <<  format<int>(11,particle.ityp())
           <<  format<int>(3,particle.iso3())
           <<  format<int>(3,particle.charge())
           <<  format<int>(9,particle.collision_parent())
           <<  format<uint>(5,particle.collisions())
           <<  format<int>(4,particle.parent_proc_id())
           << '\n';
    }
}
#endif // URQMD_EXPORTER_H
